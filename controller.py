import asyncio
import logging
import time

import serial

import exceptions

logging.basicConfig(level=logging.INFO)

class Controller:
    robot_port = None
    sensor_port = None
    panel_port = None

    def templ_autohome(self):
        self.send_to_robot_port("M40.1")
        self.send_to_robot_port("M40.2")
        self.send_to_robot_port("M81")
        # time.sleep(2)
        for code in ["G4P2000", "G28 Z", "G28 Y", "G1 Y400 F10000", "G28 A", "G1 A90 F10000", "G28 X", "G1 X400 F10000", "G1 A0"]:
            self.send_to_robot_port(code)
        self.send_to_robot_port(f"G1 X{695} Y{1130} F{120000}")

    def _connect_to_robot_port_(self):
        from serial.tools import list_ports
        for port in list_ports.comports():
            # print(port.hwid)
            # if "SER=0C001020AF4A10475D64E8D9F50020C2" in port.hwid:  # дом
            if "SER=1200900DAF6998A25E3FF6C9F50020C6" in port.hwid:
                try:
                    self.robot_port = self._create_and_open_port_(port=port.device, baudrate=115200)
                except serial.serialutil.SerialException:
                    pass

    def _connect_to_sensor_port_(self):
        from serial.tools import list_ports
        for port in list_ports.comports():
            # if "COM13" in port.device:  # дом
            if "LOCATION=1-2" in port.hwid: # Ноут
            # if "LOCATION=1-1.3" in port.hwid: # РПИ
                try:
                    self.sensor_port = self._create_and_open_port_(port=port.device, baudrate=19200)
                    self.sensor_port.read(self.sensor_port.in_waiting)
                except serial.serialutil.SerialException:
                    pass

    def _connect_to_panel_port_(self):
        from serial.tools import list_ports
        for port in list_ports.comports():
            # if "COM12" in port.device:  # дом
            if "LOCATION=1-3" in port.hwid: # Ноут
            # if "LOCATION=1-1.5" in port.hwid: #РПИ
                try:
                    self.panel_port = self._create_and_open_port_(port=port.device, baudrate=4800)
                except serial.serialutil.SerialException:
                    pass

    @staticmethod
    def _create_and_open_port_(port, baudrate):
        new_port = serial.Serial(timeout=1)
        new_port.port = port
        new_port.baudrate = baudrate
        new_port.open()
        return new_port

    async def search_robot_port(self, robot_port_found):
        logging.info('Поиск робота...')
        while not self.robot_port:
            self._connect_to_robot_port_()
            await asyncio.sleep(0.1)
        else:
            robot_port_found.set()
            logging.info("Робот найден")

    async def search_sensor_port(self, sensor_port_found):
        logging.info('Поиск сенсора...')
        while not self.sensor_port:
            self._connect_to_sensor_port_()
            await asyncio.sleep(0.1)
        else:
            sensor_port_found.set()
            logging.info("Сенсор найден")

    async def search_panel_port(self, panel_port_found):
        logging.info('Поиск панели...')
        while not self.panel_port:
            self._connect_to_panel_port_()
            await asyncio.sleep(0.1)
        else:
            panel_port_found.set()
            logging.info("панель найдена")

    def reconnect_to_robot_port(self, robot_port_found):
        self.robot_port = None
        logging.info('Поиск робота...')
        while not self.robot_port:
            self._connect_to_robot_port_()
        else:
            robot_port_found.set()
            logging.info("Робот найден")

    def reconnect_to_sensor_port(self, sensor_port_found):
        self.sensor_port = None
        logging.info('Поиск сенсора...')
        while not self.sensor_port:
            self._connect_to_sensor_port_()
        else:
            sensor_port_found.set()
            logging.info("Сенсор найден")

    def reconnect_to_panel_port(self, panel_port_found):
        self.panel_port = None
        logging.info('Поиск панели...')
        while not self.panel_port:
            self._connect_to_panel_port_()
        else:
            panel_port_found.set()
            logging.info("Панель найдена")

    async def all_ports_found(self, robot_port_found,
                              sensor_port_found, panel_port_found, system_ready,
                              panel_ready, pinging):
        while True:
            await robot_port_found.wait()
            await sensor_port_found.wait()
            await panel_port_found.wait()
            logging.info("ВСЕ УСТРОЙСТВА НАЙДЕНЫ")
            self.wait_status()
            # time.sleep(0.5)
            # self.templ_autohome()
            system_ready.set()
            panel_ready.set()
            robot_port_found.clear()
            sensor_port_found.clear()
            panel_port_found.clear()
            pinging.set()

    def send_to_robot_port(self, string):
        try:
            if "M114.2" in string or "M114" in string or "" == string or " " == string or "\n" == string:
                # print("исключение")
                pass
            else:
                pass
                named_tuple = time.localtime()
                time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
                print("код:", string, " время: ", time_string)

            # if "M114.2" not in string:
            #     print("код", string)

                # if "M114.2" != string:
                #     if "M114" != string:
                #         if "M400" != string:
                #             if "" != string:
                #                 print("код:", string)
                # if "\n" == string:
                #     print("перенос строки")
                # if " " == string:
                #     print("пробел")
                # if "" == string:
                #     print("пустота")

            self.robot_port.write(f"{string}\n".encode())
        except serial.serialutil.SerialException:
            raise exceptions.RobotOfflineException
        except Exception:
            raise exceptions.RobotOfflineException

    def read_from_robot_port(self):
        # try:
        if self.robot_port.in_waiting > 0:
            return self.robot_port.readline().decode().replace('\n', '').replace('\r', '')
        else:
            return ""
        # except serial.serialutil.SerialException:
        #     raise exceptions.RobotOfflineException
        # except Exception:
        #     raise exceptions.RobotOfflineException

    def send_to_sensor_port(self, string):
        try:
            self.sensor_port.write(f"{string}\n".encode())
            self.sensor_port.readline()
        except serial.serialutil.SerialException:
            raise exceptions.SensorOfflineException
        except Exception:
            raise exceptions.SensorOfflineException

    def read_from_sensor_port(self, line=False):
        try:
            if line:
                return self.sensor_port.readline()
            else:
                return self.sensor_port.read(self.sensor_port.in_waiting)
        except serial.serialutil.SerialException:
            raise exceptions.SensorOfflineException
        except Exception:
            raise exceptions.SensorOfflineException

    def send_to_panel_port(self, string):
        try:
            self.panel_port.write(f"{string}\n".encode())
            self.panel_port.readline()
        except serial.serialutil.SerialException:
            raise exceptions.PanelOfflineException
        except Exception:
            raise exceptions.PanelOfflineException

    def read_from_panel_port(self):
        try:
            if self.panel_port.in_waiting > 0:
                line = self.panel_port.readline()
                print("read_from_panel_port", line.decode('ascii', 'ignore'))
                if (len(line) == 39):
                    # print("________________________________________________________________")
                    try:
                        line = line.decode('ascii').replace('\n', '').replace('\r', '')
                        print("read_from_panel_port OK)", line)
                    except UnicodeDecodeError:
                        line = line.decode('ascii', 'ignore').replace('\n', '').replace('\r', '')
                        print("read_from_panel_port IGNORE", line)
                    return line
                else:
                    return ""
            else:
                # print('self.panel_port.in_waiting == 0')
                return ""
        except serial.serialutil.SerialException:
            raise exceptions.PanelOfflineException
        except Exception:
            raise exceptions.PanelOfflineException

    def clear_panel_port(self):
        try:
            self.panel_port.write("\n".encode())
            self.panel_port.write("\n".encode())
            self.panel_port.write("\n".encode())
            line = self.panel_port.read(self.panel_port.in_waiting)
        except serial.serialutil.SerialException:
            raise exceptions.PanelOfflineException
        except Exception:
            raise exceptions.PanelOfflineException

    def ping_panel_port(self):
        try:
            res = self.panel_port.in_waiting > 0
        except serial.serialutil.SerialException:
            raise exceptions.PanelOfflineException
        except Exception:
            raise exceptions.PanelOfflineException

    def wait_status(self):
        self.set_light_mode(7)  # Белый
        self.set_status_on_the_panel(1, 0, 0, 0)
        # print("wait_status")

    def in_work_status(self):
        self.set_status_on_the_panel(1, 1, 0, 0)
        self.set_light_mode(2)  # Белый
        # print("work")

    def to_home_status(self):
        self.set_light_mode(9)  # Белый
        self.set_status_on_the_panel(1, 1, 0, 0)
        # print("to_home_status")

    def scan_status(self):
        self.set_light_mode(5)  # Белый
        self.set_status_on_the_panel(1, 1, 0, 0)
        # print("scan_status")

    def sleep_status(self):
        self.set_light_mode(0)  # Белый
        self.set_status_on_the_panel(1, 0, 0, 0)
        # print("sleep_status")

    def robot_offline_error(self):
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 1, 1)
        logging.error("ROBOT OFFLINE")

    def sensor_offline_error(self):
        self.set_status_on_the_panel(1, 0, 1, 2)
        logging.error("SENSOR OFFLINE")

    def panel_offline_error(self):
        self.set_light_mode(4)
        logging.error("PANEL OFFLINE")

    def sensor_returns_nothing_error(self):
        self.set_status_on_the_panel(1, 0, 1, 3)
        self.set_light_mode(4)
        logging.error("sensor returns nothing")

    def number_of_buttons_pressed_error(self):  # разрешить пользоваться панелью
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 2, 1)
        logging.error("Больше 1 нажатой кнопки")

    def wrong_box_type_error(self):  # разрешить пользоваться панелью
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 2, 2)
        logging.error("Неправильный тип ящика")

    def boxes_not_found_error(self):  # разрешить пользоваться панелью
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 2, 3)
        logging.error("Ящик не найден")

    def high_error(self):  # разрешить пользоваться панелью
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 2, 4)
        logging.error("Превышение максимального количества ящиков по высоте")

    def need_homing_error(self):
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 2, 5)
        logging.error("Нужен хоминг")

    def box_fell_down_error(self):
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 3, 1)
        logging.error("Ошибка выпал ящик")

    def unknown_error(self):
        self.set_light_mode(4)
        self.set_status_on_the_panel(1, 0, 4, 1)
        logging.error("Неизвестная ошибка")

    def set_status_on_the_panel(self, ready, work, error_number1, error_number2):
        try:
            time.sleep(0.0002)
            self.panel_port.write("L".encode('ascii'))
            time.sleep(0.0002)
            self.panel_port.write("E".encode('ascii'))
            time.sleep(0.0002)
            self.panel_port.write("D".encode('ascii'))
            time.sleep(0.0002)
            self.panel_port.write("S".encode('ascii'))
            time.sleep(0.0002)
            self.panel_port.write(" ".encode('ascii'))
            time.sleep(0.0002)
            self.panel_port.write(str(ready).encode('ascii'))
            # print("ready",ready)
            time.sleep(0.0002)
            self.panel_port.write(",".encode('ascii'))
            time.sleep(0.0002)
            self.panel_port.write(str(work).encode('ascii'))
            # print("work",work)
            time.sleep(0.0002)
            self.panel_port.write(",".encode('ascii'))
            time.sleep(0.0002)
            a = int(error_number1) + int(error_number2)
            if int(error_number1) + int(error_number2) == 0:
                self.panel_port.write("0".encode('ascii'))
                time.sleep(0.0002)
                # print("0")
            else:
                self.panel_port.write(str(error_number1).encode('ascii'))
                time.sleep(0.0002)
                self.panel_port.write(str(error_number2).encode('ascii'))
                time.sleep(0.0002)
                # print(error_number1, error_number2)
            self.panel_port.write("\n".encode('ascii'))

            # print("ready", ready, "work", work, "er1", error_number1, "er1", error_number2)
        except serial.serialutil.SerialException:
            raise exceptions.PanelOfflineException
        except Exception:
            pass

    def set_light_mode(self, code):
        try:
            self.sensor_port.write(f"{code}\n".encode())
        except serial.serialutil.SerialException:
            raise exceptions.SensorOfflineException
        except Exception:
            raise exceptions.SensorOfflineException
