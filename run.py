import asyncio
import logging
import traceback
import time

import exceptions
from controller import Controller
from panel import Panel
from robot import Robot
from sensor import Sensor
from utils import pre_detect, detect_box, combine_boxes

logging.basicConfig(level=logging.INFO)

controller = Controller()
panel = Panel()
robot = Robot()
sensor = Sensor()


async def do_action(action, panel_ready, robot_port_found, sensor_port_found, panel_port_found, pinging):
    while True:
        await action.wait()
        # panel_ready.clear()
        pinging.clear()
        try:
            named_tuple = time.localtime()
            time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
            print("do_action 1: ", time_string)

            logging.info("В работе...")
            controller.in_work_status()
            named_tuple = time.localtime()
            time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
            print("do_action 2: ", time_string)

            robot.go_to_start()
            pre_detected_height = pre_detect(robot=robot, sensor=sensor, box_number=panel.panel_value,
                                             controller=controller)
            await asyncio.sleep(0.01)
            print("закончили pre_detect")
            if pre_detected_height:
                boxes = detect_box(box_number=panel.panel_value, robot=robot, sensor=sensor, controller=controller)
                print("Закончили detect_box")
                if boxes:
                    await asyncio.sleep(0.01)
                    combine_boxes(boxes, robot, box_number=panel.panel_value, sensor=sensor)
                    await asyncio.sleep(0.01)
                    robot.go_to_start()
                    robot.move('G1 Z0')
                    await asyncio.sleep(0.01)
                    controller.wait_status()
            logging.info("Работа завершена")
            await asyncio.sleep(0.1)
            controller.clear_panel_port()
            action.clear()
            panel_ready.set()
            pinging.set()
        except exceptions.RobotOfflineException:
            logging.error(traceback.print_exc())
            controller.robot_offline_error()
            action.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.SensorOfflineException:
            logging.error(traceback.print_exc())
            controller.sensor_offline_error()
            action.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.PanelOfflineException:
            logging.error(traceback.print_exc())
            controller.panel_offline_error()
            action.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except Exception as e:
            logging.error(traceback.print_exc())
            traceback.print_exc()
            controller.unknown_error()
            pinging.clear()
            action.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue


async def do_sleep(sleep, panel_ready,
                   robot_port_found, sensor_port_found, panel_port_found,
                   pinging):
    while True:
        await sleep.wait()
        panel_ready.clear()
        pinging.clear()
        try:
            logging.info("Start sleeping")
            await robot.sleep()
            logging.info("Finish sleeping")
            await asyncio.sleep(0.1)
            controller.clear_panel_port()
            sleep.clear()
            panel_ready.set()
            pinging.set()
        except exceptions.RobotOfflineException:
            logging.error(traceback.print_exc())
            controller.robot_offline_error()
            sleep.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.SensorOfflineException:
            logging.error(traceback.print_exc())
            controller.sensor_offline_error()
            sleep.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.PanelOfflineException:
            logging.error(traceback.print_exc())
            controller.panel_offline_error()
            sleep.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except Exception as e:
            logging.error(traceback.print_exc())
            traceback.print_exc()
            controller.unknown_error()
            pinging.clear()
            sleep.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue


async def do_homing(homing, panel_ready,
                    robot_port_found, sensor_port_found, panel_port_found,
                    pinging):
    while True:
        await homing.wait()
        panel_ready.clear()
        pinging.clear()
        try:
            logging.info("Start homing")
            await robot.homing()
            logging.info("Finish homing")
            await asyncio.sleep(0.1)
            controller.clear_panel_port()
            homing.clear()
            panel_ready.set()
            pinging.set()
        except exceptions.RobotOfflineException:
            controller.robot_offline_error()
            homing.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.SensorOfflineException:
            logging.error(traceback.print_exc())
            controller.sensor_offline_error()
            homing.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.PanelOfflineException:
            logging.error(traceback.print_exc())
            controller.panel_offline_error()
            homing.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except Exception as e:
            logging.error(traceback.print_exc())
            traceback.print_exc()
            controller.unknown_error()
            pinging.clear()
            homing.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue


async def do_changing_gripper(changing_gripper, panel_ready,
                              robot_port_found, sensor_port_found, panel_port_found,
                              pinging):
    while True:
        await changing_gripper.wait()
        panel_ready.clear()
        pinging.clear()
        try:
            logging.info("Start change of gripper")
            await robot.change_of_gripper()
            logging.info("Finish change of gripper")
            await asyncio.sleep(0.1)
            controller.clear_panel_port()
            changing_gripper.clear()
            panel_ready.set()
            pinging.set()
        except exceptions.RobotOfflineException:
            logging.error(traceback.print_exc())
            controller.robot_offline_error()
            changing_gripper.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.SensorOfflineException:
            logging.error(traceback.print_exc())
            controller.sensor_offline_error()
            changing_gripper.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.PanelOfflineException:
            logging.error(traceback.print_exc())
            controller.panel_offline_error()
            changing_gripper.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except Exception as e:
            logging.error(traceback.print_exc())
            traceback.print_exc()
            controller.unknown_error()
            pinging.clear()
            changing_gripper.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue


async def ping(system_ready, pinging, panel_ready,
               robot_port_found, sensor_port_found, panel_port_found, ):
    while True:
        try:
            await system_ready.wait()
            await pinging.wait()
            await asyncio.sleep(0.01)
            robot.ping_port()
            sensor.ping_port()

        except exceptions.RobotOfflineException:
            logging.error(traceback.print_exc())
            controller.robot_offline_error()
            pinging.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.SensorOfflineException:
            logging.error(traceback.print_exc())
            controller.sensor_offline_error()
            pinging.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except exceptions.PanelOfflineException:
            logging.error(traceback.print_exc())
            controller.panel_offline_error()
            pinging.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue
        except Exception as e:
            logging.error(traceback.print_exc())
            logging.error(e)
            traceback.print_exc()
            controller.unknown_error()
            pinging.clear()
            controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
            controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
            controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
            panel_ready.set()
            continue


async def main():
    robot_port_found = asyncio.Event()
    sensor_port_found = asyncio.Event()
    panel_port_found = asyncio.Event()
    system_ready = asyncio.Event()
    panel_ready = asyncio.Event()
    action = asyncio.Event()
    sleep = asyncio.Event()
    homing = asyncio.Event()
    changing_gripper = asyncio.Event()
    pinging = asyncio.Event()

    search_robot_port_task = asyncio.create_task(controller.search_robot_port(robot_port_found=robot_port_found))
    search_sensor_port_task = asyncio.create_task(controller.search_sensor_port(sensor_port_found=sensor_port_found))
    search_panel_port_task = asyncio.create_task(controller.search_panel_port(panel_port_found=panel_port_found))



    all_devices_found_task = asyncio.create_task(controller.all_ports_found(
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
        system_ready=system_ready,
        panel_ready=panel_ready,
        pinging=pinging
    ))
    get_panel_value_task = asyncio.create_task(panel.get_state(
        system_ready=system_ready,
        panel_ready=panel_ready,
        action=action,
        homing=homing,
        sleep=sleep,
        changing_gripper=changing_gripper,
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
        pinging=pinging
    ))
    do_action_task = asyncio.create_task(do_action(
        action=action,
        panel_ready=panel_ready,
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
        pinging=pinging,
    ))
    do_sleep_task = asyncio.create_task(do_sleep(
        sleep=sleep,
        panel_ready=panel_ready,
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
        pinging=pinging
    ))
    do_homing_task = asyncio.create_task(do_homing(
        homing=homing,
        panel_ready=panel_ready,
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
        pinging=pinging
    ))
    do_changing_gripper_task = asyncio.create_task(do_changing_gripper(
        changing_gripper=changing_gripper,
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
        panel_ready=panel_ready,
        pinging=pinging
    ))
    ping_task = asyncio.create_task(ping(
        system_ready=system_ready,
        pinging=pinging,
        panel_ready=panel_ready,
        robot_port_found=robot_port_found,
        sensor_port_found=sensor_port_found,
        panel_port_found=panel_port_found,
    ))

    await search_robot_port_task
    await search_sensor_port_task
    await search_panel_port_task
    await all_devices_found_task
    await get_panel_value_task
    await do_action_task
    await do_sleep_task
    await do_homing_task
    await do_changing_gripper_task
    await ping_task


if __name__ == '__main__':
    robot.run(controller=controller)
    sensor.run(controller=controller)
    panel.run(controller=controller, robot=robot)

    asyncio.run(main())
