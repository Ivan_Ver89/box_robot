class RobotOfflineException(Exception):
    pass


class SensorOfflineException(Exception):
    pass


class PanelOfflineException(Exception):
    pass


class ReloadException(Exception):
    pass


class WrongNumberOfButtons(Exception):
    pass
