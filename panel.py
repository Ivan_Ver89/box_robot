import asyncio
import logging
import traceback
import time
from collections import Counter

import exceptions


class Panel:
    panel_value = None

    def run(self, controller, robot):
        self.controller = controller
        self.robot = robot

    def ping_port(self):
        self.controller.ping_panel_port()


    async def get_state(self, system_ready, panel_ready, action, sleep, homing, changing_gripper,
                        robot_port_found, sensor_port_found, panel_port_found,
                        pinging):
        while True:
            # print("get_state_panel------------------------------------------")
            try:
                # print("try1")
                await system_ready.wait()
                # print("try2")
                await asyncio.sleep(0.01)
                # # print("----------------------------------------------------------------------------------------")
                # # print("принт 0")
                # print("try3")
                line2 = self.controller.read_from_panel_port()
                # print("Вернулись в панель line2=", line2)
                # print("принт 1")
                # if line2 !="":
                #     print("Перед условием ", line2)
                #     print("line2 != PANEL_PING\r\n", line2 != "PANEL_PING\r\n")
                #     print("line2 != OK_LEDS\r\n", line2 != "OK_LEDS\r\n")

                if line2 != "OK_LEDS\r\n" and line2 != "" and line2 != "PANEL_PING\r\n":
                    # print("После условия ", line2)
                    if panel_ready.is_set():
                        line2 = line2.replace("\n", "").replace("\r", "")
                        buttons = line2.split(" ")[2].split(",")
                        c = Counter(buttons)
                        print(buttons)

                        named_tuple = time.localtime()
                        time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
                        print("ОТМЕТКА ВРЕМЕНИ 1: ", time_string)

                        if c['1'] == 0:
                            homing.set()
                        elif self.robot.get_current_coordinates() != [0, 0, 0]:
                            if c['1'] > 1:
                                if [i for i, v in enumerate(buttons) if v == '1'] == [9, 10]:
                                    changing_gripper.set()
                                    print("проверка")
                                else:
                                    # print("c['1']", c['1'])
                                    self.controller.number_of_buttons_pressed_error()
                            else:
                                if buttons.index("1") == 0:
                                    sleep.set()
                                else:
                                    self.panel_value = buttons.index("1")
                                    panel_ready.clear()
                                    action.set()
                        else:
                            self.controller.need_homing_error()
                    else:
                        logging.warning("Панель занята")
                else:
                    pass
            except exceptions.PanelOfflineException:
                pinging.clear()
                logging.error(traceback.print_exc())
                self.controller.panel_offline_error()
                self.controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
                self.controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
                self.controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
                panel_ready.set()
                continue
            except Exception as e:
                logging.error(traceback.print_exc())
                self.controller.unknown_error()
                pinging.clear()
                self.controller.reconnect_to_robot_port(robot_port_found=robot_port_found)
                self.controller.reconnect_to_sensor_port(sensor_port_found=sensor_port_found)
                self.controller.reconnect_to_panel_port(panel_port_found=panel_port_found)
                panel_ready.set()
                continue
