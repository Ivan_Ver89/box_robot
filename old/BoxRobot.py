import time

import pwlf
import serial
from sympy import *
from threading import Thread
import matplotlib.pyplot as plt
import logging

buttons = {
    "1": {"L": 600, "W": 400, "H": 115, "delta": 115, "inner": True, "perimeter": True, "blow": False},
    "2": {"L": 600, "W": 400, "H": 118, "delta": 188, "inner": True, "perimeter": True, "blow": False},
    "3": {"L": 600, "W": 400, "H": 183, "delta": 183, "inner": False, "perimeter": True, "blow": False},
    "4": {"L": 600, "W": 400, "H": 110, "delta": 110, "inner": False, "perimeter": True, "blow": False},
    "5": {"L": 600, "W": 400, "H": 188, "delta": 188, "inner": True, "perimeter": True, "blow": False},
    "6": {"L": 540, "W": 340, "H": 298, "delta": 80, "inner": True, "perimeter": False, "blow": True},
    "7": {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "perimeter": False, "blow": False},
    "8": {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "perimeter": False, "blow": False},
}

test_boxes = {
    "coords": [[300, 900], [900, 900], [300, 300], [900, 300]]
}


class Box:
    cx = None
    cy = None
    cz = None
    alpha = None

    def __init__(self, cx, cy, cz, alpha):
        self.cx = cx
        self.cy = cy
        self.cz = cz
        self.alpha = alpha


class BoxRobot:
    g_code_list = None

    box = None
    is_moving = False
    coords_and_height = list()

    F_up = 30000    # Скорость вверх
    F_down = 60000  # Скорость вниз
    F_xy = 120000    # Скорость по XY

    H_vacuum = 1000 # Высота присоски
x 19cm +
    def __init__(self):
        self.g_code_list = {
            "homing": ["M81\n", "G28 Y\n", "G28 X\n", "G28 Z\n"],
            "to_start": f"G1 X550 Y1300 {self.F_xy}",
            "to_scan": [
                f"G1 X10 Y700 {self.F_xy}",
                f"G1 X450 Y50 {self.F_xy}",
                f"G1 X1400 Y600 {self.F_xy}",
                f"G1 X750 Y1100 {self.F_xy}"
            ],
            "to_move": [
                f"G1 X450 Y1100 {self.F_xy}",
                f"G1 X10 Y600 {self.F_xy}",
                f"G1 X750 Y50 {self.F_xy}",
                f"G1 X1400 Y700 {self.F_xy}",
            ]
        }
        # com-port робота
        self.robot_port = serial.Serial()
        self.robot_port.port = 'COM6'
        self.robot_port.baudrate = 9600
        # # com-port датчиков
        # self.range_finder_port = serial.Serial()
        # self.range_finder_port.port = 'COM1'
        # self.range_finder_port.baudrate = 4800
        # # com-port панели управления
        # self.control_panel_port = serial.Serial()
        # self.control_panel_port.port = 'COM3'
        # self.control_panel_port.baudrate = 4800

    def run(self):
        if not self.robot_port.is_open:
            self.robot_port.open()
        # if not self.range_finder_port.is_open:
        #     self.range_finder_port.open()
        # if not self.control_panel_port.is_open:
        #     self.control_panel_port.open()
        for i in self.g_code_list["homing"]:
            self.move(i)
        self.move(self.g_code_list["to_start"])
        self.box = {"L": 600, "W": 400, "H": 183, "delta": 183, "inner": False, "perimeter": True, "blow": False}
        self.start_scan()
        # Thread(target=self.listen_buttons).start()

    def start_scan(self):
        # for z in range(0, 500, self.box["delta"]):
        self.z = 0
        while self.z < 1500:
            self.move(f"G1 Z{self.z} {self.F_down}")
            for i, i_code in enumerate(self.g_code_list["to_move"]):
                # border_coords = list()
                # self.coords_and_height.clear()
                self.move(i_code)
                self.move(self.g_code_list["to_scan"][i])
                # self.combine_data(self.g_code_list["to_scan"][i])
                # border_coords.append((self.coords_and_height[0]["x1"],
                #                       self.get_border_points([k["y1"] for k in self.coords_and_height],
                #                                              [k["h1"] for k in self.coords_and_height])[2]
                #                       ))
                #
                # border_coords.append((self.coords_and_height[0]["x2"],
                #                       self.get_border_points([k["y2"] for k in self.coords_and_height],
                #                                              [k["h2"] for k in self.coords_and_height])[2]
                #                       ))

            self.move(self.g_code_list["to_start"])
            if self.z > 500:
                for n, j in enumerate(test_boxes["coords"]):
                    self.move(f"G1 Z{self.z - 100} {self.F_up}")                  # Поднялся на 10 см
                    self.move(f"G1 X{j[0]} Y{j[1]} {self.F_xy}")                  # Пришел в координаты
                    self.move(f"G1 Z{self.z + 100} {self.F_down}")                # Опустился

                    # self.move("")                                               # G-код для включения присосок

                    self.move(f"G1 Z{self.z + 100 - self.box['H']} {self.F_up}")  # Поднялся на высоту коробки
                    self.move(f"G1 Y1300 {self.F_xy}")                            # В стартовое положение
                    self.move(f"G1 X550 {self.F_xy}")                             # В стартовое положение
                    self.move(f"G1 Z{1500 - self.box['H'] - self.H_vacuum} {self.F_down}")  # Опустился на высоту мойки
                    self.move(f"G1 Y1700 {self.F_xy}")                            # Двинулся в мойку
                    self.move(f"G1 Z{1500 - self.box['H'] + 20} {self.F_down}")   # Опустил коробку

                    # self.move("")                                               # G-код для выключения присосок

                    time.sleep(0.5)
                    self.move(f"G1 Z{1500 - self.box['H'] - self.H_vacuum} {self.F_up}")
                    self.move(self.g_code_list["to_start"])                       # В стартовое положение
                    if n == 3:
                        self.z += self.box["delta"]
                    self.move(f"G1 Z{self.z} {self.F_up}")                        # Пошел на верх
            else:
                self.z += self.box["delta"]

    # def combine_boxes(self):

    def move(self, g_code):
        self.is_moving = True
        self.robot_port.write(f"{g_code}\nM400\n".encode('ascii'))
        logging.warning(g_code)
        response = ""
        while response != "M400ok":
            response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
        else:
            logging.warning(g_code + " - " + response)
            self.is_moving = False

    def combine_data(self, j_code):
        self.coords_and_height.clear()
        finish_coords = (float(j_code.split(" ")[1][1:]), float(j_code.split(" ")[2][1:]))
        self.robot_port.write(f"{j_code}\n".encode('ascii'))
        current_coords = None
        while current_coords != finish_coords:
            self.robot_port.write(f"M114.2\n".encode('ascii'))
            robot_line = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            sensor_line = self.range_finder_port.read(self.range_finder_port.inWaiting()).decode('ascii')
            # box_is_find =
            if "L" in sensor_line:

                try:
                    heights = sensor_line.split("\r\n")[-2].split(" ")
                    if 8191 not in [float(i) for i in heights]:
                        self.coords_and_height.append({
                            "x1": float(robot_line.split(" ")[2][2:]) + 50,
                            "y1": float(robot_line.split(" ")[3][2:]) - 300,
                            "h1": float(heights[1]),
                            "x2": float(robot_line.split(" ")[2][2:]) - 50,
                            "y2": float(robot_line.split(" ")[3][2:]) - 300,
                            "h2": float(heights[2]),
                        })
                        current_coords = (
                            round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))
                    time.sleep(0.02)
                except:
                    pass

    def listen_buttons(self):
        while True:
            response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            if response and not self.is_moving:
                self.box = buttons[response]

    @staticmethod
    def get_border_points(coord, h):
        plt.plot(coord, h, "o")
        my_pwlf = pwlf.PiecewiseLinFit(coord, h)
        breaks = my_pwlf.fit(3)
        plt.show()
        return list(breaks)

    @staticmethod
    def get_geometry(width, length, points: list, i):
        if i == 0 or i == 2:
            ptA = points[0]
            ptB = points[1]
            ptC = points[2]
        if i == 1:
            ptA = points[1]
            ptB = points[2]
            ptC = points[0]

        if ptA[1] >= ptB[1]:
            print("Точка A должна располагаться ниже точки B (координаты получены некорректно).")
            print("Координаты точки A -", ptA)
            print("Координаты точки B -", ptB)
            print("Продолжаю вычисления... (результаты могут быть некорректны)")
        line1 = Line(ptA, ptB)
        line2 = line1.perpendicular_line(ptC)
        ptE = line1.intersection(line2)
        c1 = Circle(ptE[0].evalf(), width)
        points1 = line2.intersection(c1)
        ptM = points1[0]
        c2 = Circle(ptE[0].evalf(), length)
        points2 = line1.intersection(c2)
        if ptA[0] > ptB[0]:
            ptK = points2[1]
        else:
            ptK = points2[0]
        line3 = line1.parallel_line(ptM)
        line4 = line2.parallel_line(ptK)
        box_angle = atan(line1.slope)
        if box_angle < 0:
            box_angle += pi
        ptH = line3.intersection(line4)
        ptG = Segment(ptE[0].evalf(), ptH[0].evalf()).midpoint
        if i == 0 or i == 2:
            return float(box_angle), float(ptG.x), float(ptG.y - width)
        if i == 1:
            return float(box_angle), float(ptG.x), float(ptG.y)


if __name__ == '__main__':
    robot = BoxRobot()
    robot.run()
