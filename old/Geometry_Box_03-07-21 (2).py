from sympy.geometry import *
from old.math import degrees,atan,pi
import matplotlib.pyplot as plt

print("Добро пожаловать в 'Расчет Ящиков'! (Версия от 03.07.2021)")
print("Для расчета углов, центра и угла наклона ящика необходимо указать его габариты и координаты 3 точек:")
print("Точки A и B должны лежать на одной стороне ящика, а точка C на стороне, перпендикулярной первой.")
print("Ширина - это сторона ящика, у которой известна 1 точка (Точка C).")
print("Длина - это сторона ящика, у которой известно 2 точки (Точки A, B).")

# Задаем параметры ящика
width = 400
length = 600

# Получаем три точки с датчика: две на первой стороне, одну на перпендикулярной ей
#centr
# ptA = [430, 800]
# ptB = [130, 800]
# ptC = [17, 500]

#right
ptA = [1271.734, 848.993]
ptB = [989.826, 746.387]
ptC = [986.246, 425.831]

# #left
# ptA = [-428.004,771.697]
# ptB = [-709.912, 874.303]
# ptC = [-918.703,631.043]

# #centr2
# ptA = [430.0,-314.558]
# ptB = [130.0,-314.558]
# ptC = [17.0,-14.558]

# #right2
# ptA = [1412.962,-261.765]
# ptB = [1131.054,-364.371]
# ptC = [922.263,-121.112]

# #left2
# ptA = [-555.333,-344.121]
# ptB = [-837.241,-241.515]
# ptC = [-840.82,79.041]









print("Введены следующие координаты исходных точек:")
print("Точка A: ", ptA)
print("Точка B: ", ptB)
print("Точка C: ", ptC)

# Создаем прямую AB, проходящую через точки A и B
line1 = Line(ptA,ptB)

# Создаем прямую CE, проходящую через точку C перпендикулярно line1
line2 = line1.perpendicular_line(ptC)

# В месте пересечения прямых line1 и line2 обозначаем точку E
ptE = line1.intersection(line2)
print("Найдены следующие координаты угловых точек:")
print("Координаты точки E -", ptE[0].evalf())

# Проводим луч из точки E в направлении точки C
rayEC = Ray(ptE[0], ptC)

# Создаем окружность с центром в точке E и радиусом, равным ширине ящика
c1 = Circle(ptE[0], width)

# Находим точку M пересечения окружности c1 с лучом rayEC
#points1 = line2.intersection(c1)
#ptM = points1
ptM = rayEC.intersection(c1)
print("Координаты точки M -", ptM[0].evalf())

# Создаем окружность с центром в точке E и радиусом, равным длине ящика
c2 = Circle(ptE[0], length)

# Проводим луч из точки E в направлении точки A
rayEA = Ray(ptE[0], ptA)

# Находим точку K пересечения окружности c2 с лучом rayEA
ptK = rayEA.intersection(c2)
print("Координаты точки K -", ptK[0].evalf())

# Создаем прямые, параллельные line1 и line2, проходящие через точки M и K соответственно
line3 = line1.parallel_line(ptM[0])
line4 = line2.parallel_line(ptK[0])

# В точке пересечения прямых line3 и line4 обозначаем точку H
ptH = line3.intersection(line4)
print("Координаты точки H -", ptH[0].evalf())

# В качестве центра ящика берем середину отрезка EH
ptG = Segment(ptE[0].evalf(),ptH[0].evalf()).midpoint
print("Координата центра ящика G -", ptG.evalf())

# Угол относительно оси X
box_angle = atan(line1.slope)
if box_angle < 0:
    box_angle += pi

print("Угол наклона ящика :", degrees(box_angle), "° (",box_angle, "рад )*")

print("* Отсчет угла ведется от положительного направления оси X против часовой стрелки: +X = 0°, +Y = 90°, -X=180°.")


plt.scatter(x=float(ptE[0].x), y=float(ptE[0].y), c="green", edgecolors="green")
plt.plot([float(ptE[0].x), float(ptM[0].x)], [float(ptE[0].y), float(ptM[0].y)], c="green")

plt.scatter(x=float(ptM[0].x), y=float(ptM[0].y), c="green", edgecolors="green", alpha=0.6)
plt.plot([float(ptM[0].x), float(ptH[0].x)], [float(ptM[0].y), float(ptH[0].y)], c="green")

plt.scatter(x=float(ptH[0].x), y=float(ptH[0].y), c="green", edgecolors="green", alpha=0.6)
plt.plot([float(ptH[0].x), float(ptK[0].x), ], [float(ptH[0].y), float((ptK[0].y)), ], c="green")

plt.scatter(x=float(ptK[0].x), y=float(ptK[0].y), c="green", edgecolors="green", alpha=0.6)
plt.plot([float(ptK[0].x), float(ptE[0].x)], [float(ptK[0].y), float(ptE[0].y)], c="green")

plt.scatter(x=float(ptA[0]), y=float(ptA[1]), c="red", edgecolors="red")
plt.scatter(x=float(ptB[0]), y=float(ptB[1]), c="red", edgecolors="red")
plt.scatter(x=float(ptC[0]), y=float(ptC[1]), c="red", edgecolors="red")

plt.scatter(x=float(ptG.x), y=float(ptG.y), c="red", edgecolors="blue")
# plt.scatter(ptM, 'O')
# plt.scatter(ptK, 'O')
# plt.scatter(ptH, 'O')
plt.show()