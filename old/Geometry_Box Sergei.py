from sympy import *
from old.math import degrees,atan
import matplotlib.pyplot as plt
# Задаем параметры ящика
# width = input("Введите ширину ящика: ")
# length = input("Введите длину ящика: ")
width = 400
length = 600

# Получаем три точки с датчика: две на первой стороне, одну на перпендикулярной ей
# ptA = [430, 800]
# ptB = [130, 800]
# ptC = [17, 500]
# ptA = [519.9527, 916.2009]
# ptB = [229.9791, 914.6259]
# ptC = [65.2213, 699.9757]

# ptA = [519.9527, 913.726]
# ptB = [229.9791, 915.0759]
# ptC = [64.6532, 699.9757]

# {'x': 519.9527, 'y': 916.3134, 'h': 392.0, 'delta': 288.0}
# {'x': 229.9791, 'y': 912.2634, 'h': 363.0, 'delta': 271.0}
# {'x': 69.1982, 'y': 699.9757, 'h': 401.0, 'delta': 310.0}

# ptA = [519.9527, 916.3134]
# ptB = [229.9791, 912.2634]
# ptC = [69.1982, 699.9757]

ptA = [519.9527, 916.3134]
ptB = [229.9791, 912.2634]
ptC = [69.1982, 699.9757]

# Базовая проверка корректности полученных точек
if ptA[1] >= ptB[1]:
    print("Точка A должна располагаться ниже точки B (координаты получены некорректно).")
    print("Координаты точки A -", ptA)
    print("Координаты точки B -", ptB)
    print("Продолжаю вычисления... (результаты могут быть некорректны)")

# Создаем прямую, проходящую через точки A и B
line1 = Line(ptA,ptB)

# Создаем прямую, проходящую через точку C перпендикулярно line1
line2 = line1.perpendicular_line(ptC)

# В месте пересечения прямых line1 и line2 обозначаем точку E
ptE = line1.intersection(line2)

# Создаем окружность с центром в точке E и радиусом, равным ширине ящика
c1 = Circle(ptE[0].evalf(), width)

# Находим точку пересечения окружности c1 с прямой line2
points1 = line2.intersection(c1)
ptM = points1[0]

# Создаем окружность с центром в точке E и радиусом, равным длине ящика
c2 = Circle(ptE[0].evalf(), length)

# Находим точку пересечения окружности c2 с прямой line1
points2 = line1.intersection(c2)
if ptA[0] > ptB[0]:
    ptK = points2[1]
else:
    ptK = points2[0]

# Создаем прямые, параллельные line1 и line2, проходящие через точки M и K соответственно
line3 = line1.parallel_line(ptM)
line4 = line2.parallel_line(ptK)

# В точке пересечения прямых line3 и line4 обозначаем точку H
ptH = line3.intersection(line4)

# В качестве центра ящика берем середину отрезка EH
ptG = Segment(ptE[0].evalf(),ptH[0].evalf()).midpoint

# Угол относительно оси X
box_angle = atan(line1.slope)
if box_angle < 0:
    box_angle += pi

print("Координаты точки ЛВ E -", ptE[0].evalf())
print("Координаты точки ЛН M -", ptM.evalf())
print("Координаты точки ПВ K -", ptK.evalf())
print("Координаты точки ПН H -", ptH[0].evalf())
print("Координаты точки G -", ptG.evalf())
# print("Угол наклона ящика -", degrees(box_angle), "° (",box_angle, "рад ): +X = 0°, +Y = 90°, -X=180°.")
print("Угол наклона ящика -", degrees(box_angle), "° : +X = 0°, +Y = 90°, -X=180°.")

plt.scatter(x=float(ptE[0].x), y=float(ptE[0].y), c="green", edgecolors="green")
plt.plot([float(ptE[0].x), float(ptM.x)], [float(ptE[0].y), float(ptM.y)], c="green")

plt.scatter(x=float(ptM.x), y=float(ptM.y), c="green", edgecolors="green", alpha=0.6)
plt.plot([float(ptM.x), float(ptH[0].x)], [float(ptM.y), float(ptH[0].y)], c="green")

plt.scatter(x=float(ptH[0].x), y=float(ptH[0].y), c="green", edgecolors="green", alpha=0.6)
plt.plot([float(ptH[0].x), float(ptK.x), ], [float(ptH[0].y), float((ptK.y)), ], c="green")

plt.scatter(x=float(ptK.x), y=float(ptK.y), c="green", edgecolors="green", alpha=0.6)
plt.plot([float(ptK.x), float(ptE[0].x)], [float(ptK.y), float(ptE[0].y)], c="green")

plt.scatter(x=float(ptA[0]), y=float(ptA[1]), c="red", edgecolors="red")
plt.scatter(x=float(ptB[0]), y=float(ptB[1]), c="red", edgecolors="red")
plt.scatter(x=float(ptC[0]), y=float(ptC[1]), c="red", edgecolors="red")

plt.scatter(x=float(ptG.x), y=float(ptG.y), c="red", edgecolors="blue")
# plt.scatter(ptM, 'O')
# plt.scatter(ptK, 'O')
# plt.scatter(ptH, 'O')
plt.show()