import serial
import time
import matplotlib.pyplot as plt
import pwlf
import re
# import numpy as np
from scipy import optimize
import numpy as np
# import matplotlib.pyplot as plt
from scipy import interpolate

# X640
# X46

def piecewise_linear(x, x0, y0, k1, k2):
    return np.piecewise(x, [x < x0], [lambda x:k1*x + y0-k1*x0, lambda x:k2*x + y0-k2*x0])



def get_border_points(coord, heigths):
    # plt.plot(coord, h, "-")
    # my_pwlf = pwlf.PiecewiseLinFit(coord, h)
    # breaks = my_pwlf.fit(4)
    # plt.plot(breaks, [100, 100, 100, 100, 100,], "o")
    # print(breaks)
    # plt.show()
    # return list(breaks)
    coods = []
    for i in range(1, len(heigths)):
        prev_h = heigths[i - 1]
        # print(heigths[i])
        if heigths[i] - prev_h > 250:
            coods.append(heigths[i])
            break
    # print(coods)


# com-port робота
robot_port = serial.Serial()
robot_port.port = 'COM16'
robot_port.baudrate = 9600
# com-port дальномера
sensor_port = serial.Serial()
sensor_port.port = 'COM17'
sensor_port.baudrate = 115200

pattern = "L:\s\d+\r\n"

coords_and_height = list()

if not robot_port.is_open:
    robot_port.open()
if not sensor_port.is_open:
    sensor_port.open()

F_xy = 20000     # Скорость
height = 2000   # Высота

start = f"G1 X230 Y1080 Z{height} F{F_xy}"
finish = f"G1 X230 Y250 Z{height} F{F_xy}"

start_time = time.time()

robot_port.write(f"{start}\nM400\n".encode('ascii'))
response = ""
while response != "M400ok":
    response = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    sensor_line = sensor_port.read(sensor_port.in_waiting)


finish_coords = (float(finish.split(" ")[1][1:]), float(finish.split(" ")[2][1:]))
current_coords = None

robot_port.write(f"{finish}\n".encode('ascii'))
ll = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')

while current_coords != finish_coords:
    robot_port.write(f"M114.2\n".encode('ascii'))
    robot_line = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    sensor_line = sensor_port.read(sensor_port.in_waiting)

    try:

        sensor_line = sensor_line.decode('UTF-8')
        # sensor_line = "L: 365\r\n"
        res = re.findall(pattern, sensor_line)
        if len(res) > 0:
            coords_and_height.append({
                # "x": float(robot_line.split(" ")[2][2:]),
                "y": float(robot_line.split(" ")[3][2:]) - 120,
                "h": float(res[-1].split(" ")[1]),
                # "time": time.time(),
                # "delta_time": time.time() - start_time
            })
            print(
                # float(robot_line.split(" ")[2][2:]),
                float(robot_line.split(" ")[3][2:]),
                # sensor_line,
                float(res[-1].split(" ")[1])
                # time.time(),
                # time.time() - start_time
            )
        else:
            print(
                # float(robot_line.split(" ")[2][2:]),
                float(robot_line.split(" ")[3][2:]),
                # sensor_line,
                "error"
                # time.time(),
                # time.time() - start_time
            )
        current_coords = (round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))
    except Exception:
        print(
            # float(robot_line.split(" ")[2][2:]),
            float(robot_line.split(" ")[3][2:]),
            # sensor_line,
            "error"
            # time.time(),
            # time.time() - start_time
        )
    time.sleep(0.01)

# h = [i["h"] for i in coords_and_height]
# y = [i["y"] for i in coords_and_height]
# get_border_points(y, h)

# print(coords_and_height)



