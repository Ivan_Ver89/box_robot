import logging
import time

import matplotlib.pyplot as plt
import pwlf
import serial
from sympy import *

BOXES_TYPES = {
    1: {"L": 600, "W": 400, "H": 115, "delta": 115, "inner": True, "perimeter": True, "blow": False},
    2: {"L": 600, "W": 400, "H": 118, "delta": 188, "inner": True, "perimeter": True, "blow": False},
    3: {"L": 600, "W": 400, "H": 183, "delta": 183, "inner": False, "perimeter": True, "blow": False},
    4: {"L": 600, "W": 400, "H": 110, "delta": 110, "inner": False, "perimeter": True, "blow": False},
    5: {"L": 600, "W": 400, "H": 188, "delta": 188, "inner": True, "perimeter": True, "blow": False},
    6: {"L": 540, "W": 340, "H": 298, "delta": 80, "inner": True, "perimeter": False, "blow": True},
    7: {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "perimeter": False, "blow": False},
    8: {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "perimeter": False, "blow": False},
}


class Pallet:
    BOX_TYPE = None
    pallet_h = 0
    info_boxes = [
        {"number": None, "angle": None, "coords": [None, None]},
        {"number": None, "angle": None, "coords": [None, None]},
        {"number": None, "angle": None, "coords": [None, None]},
        {"number": None, "angle": None, "coords": [None, None]},
    ]

    def __init__(self, box_type):
        self.BOX_TYPE = BOXES_TYPES[box_type]

    def set_box_info(self, n, number, *args):
        self.info_boxes[n]["number"] = number
        self.info_boxes[n]["angle"] = args[0]
        self.info_boxes[n]["coords"] = [args[1], args[2]]

    def __str__(self):
        return str(self.info_boxes)


class PortalRobot:
    F_xy = 30000  # Скорость по XY
    F_down = 30000  # Скорость вниз
    test_height = 1600  # Тестовая высота
    pallet = None
    coords_and_height = list()
    total_namber_of_boxes = 0
    max_hight = 2300
    homing = ["M81", "G28 X F6000", "G28 Y F6000", "G28 Z F6000"]

    def __init__(self):
        self.g_codes = [
            [f"G1 X350 Y900 F{self.F_xy}", f"G1 X0 Y900 F{self.F_xy}"],
            # ["G1 X350 Y500 F2000", "G1 X350 Y100 F2000"],
            # ["G1 X900 Y500 F2000", "G1 X1300 Y500 F2000"],
            # ["G1 X900 Y900 F2000", "G1 X900 Y1400 F2000"]
        ]
        self.next_g_code = [
            f"G1 X0 Y500 F{self.F_xy}\nM400",
            # "G1 X900 Y100 F8000\nM400",
            # "G1 X1300 Y900 F8000\nM400",
            # "G1 X350 Y1400 F8000\nM400"
        ]
        self.to_start = f"G1 X675 Y1300 F{self.F_xy}"
        # com-port робота
        self.robot_port = serial.Serial()
        self.robot_port.port = 'COM6'
        self.robot_port.baudrate = 9600
        # com-port дальномера
        self.sensor_port = serial.Serial()
        self.sensor_port.port = 'COM15'
        self.sensor_port.baudrate = 115200

    def run(self):
        if not self.robot_port.is_open:
            self.robot_port.open()
        if not self.sensor_port.is_open:
            self.sensor_port.open()
        # for g_code in self.homing:
        #     self.move(g_code)
        self.move(self.to_start)
        self.move(f"G1 Z{self.test_height}")

    def move(self, g_code):
        self.is_moving = True
        self.robot_port.write(f"{g_code}\nM400".encode('ascii'))
        logging.warning(g_code)
        response = ""
        while response != "M400ok":
            response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
        else:
            logging.warning(g_code + " - " + response)
            self.is_moving = False

    def get_pallet_info(self, box_number):
        self.pallet = Pallet(box_type=box_number)
        for i, i_code in enumerate(self.g_codes):
            border_coords = list()
            self.coords_and_height.clear()
            for j, j_code in enumerate(i_code):
                self.combine_data(j_code)
                if (i, j) == (0, 0) or (i, j) == (1, 1) or (i, j) == (2, 0) or (i, j) == (3, 1):
                    border_coords.append((self.coords_and_height[0]["x1"],
                                          self.get_border_points([k["y1"] for k in self.coords_and_height],
                                                                 [k["h1"] for k in self.coords_and_height])[2]
                                          ))
                elif (i, j) == (0, 1) or (i, j) == (1, 1):
                    border_coords.append((self.get_border_points([k["x1"] for k in self.coords_and_height],
                                                                 [k["h1"] for k in self.coords_and_height])[1],
                                          self.coords_and_height[0]["y1"]
                                          ))
                else:
                    border_coords.append((self.get_border_points([k["x1"] for k in self.coords_and_height],
                                                                 [k["h1"] for k in self.coords_and_height])[1],
                                          self.coords_and_height[0]["y1"],
                                          ))

            self.robot_port.write(f"{self.next_g_code[i]}\n".encode('ascii'))
            resp = ""
            while resp != "M400ok":
                resp = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            self.pallet.set_box_info(i, self.get_box_count(),
                                     *self.get_geometry(length=self.pallet.BOX_TYPE["L"],
                                                        width=self.pallet.BOX_TYPE["W"],
                                                        points=border_coords, i=i))
        for i in self.pallet.info_boxes:
            self.total_namber_of_boxes += i["number"]
        print(self.pallet)

    def catch_boxes(self):
        for box in self.pallet.info_boxes:
            self.robot_port.write(f"G1 X{box[0]} Y{box[1]}\nM400\n".encode('ascii'))
            response = ""
            while response != "M400ok":
                response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            print(f"Координаты центра: {box}")

    def combine_data(self, j_code):
        self.coords_and_height.clear()
        finish_coords = (float(j_code.split(" ")[1][1:]), float(j_code.split(" ")[2][1:]))
        self.robot_port.write(f"{j_code}\n".encode('ascii'))
        response = ""
        current_coords = None
        while current_coords != finish_coords:
            self.robot_port.write(f"M114.2\n".encode('ascii'))
            robot_line = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            sensor_line = str(self.sensor_port.readline())
            try:
                if "L" in sensor_line:
                    self.coords_and_height.append({
                        "x1": float(robot_line.split(" ")[2][2:]) - 100,
                        "y1": float(robot_line.split(" ")[3][2:]),
                        "h1": float(sensor_line.split(" ")[-1][0:-5]),
                    })
                    current_coords = (
                        round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))
                time.sleep(0.02)
            except:
                pass

    @staticmethod
    def get_border_points(coord, h):
        plt.plot(coord, h, "o")
        my_pwlf = pwlf.PiecewiseLinFit(coord, h)
        breaks = my_pwlf.fit(3)
        plt.show()
        return list(breaks)

    @staticmethod
    def get_geometry(width, length, points: list, i):
        if i == 0 or i == 2:
            ptA = points[0]
            ptB = points[1]
            ptC = points[2]
        if i == 1:
            ptA = points[1]
            ptB = points[2]
            ptC = points[0]

        # if ptA[1] >= ptB[1]:
        #     print("Точка A должна располагаться ниже точки B (координаты получены некорректно).")
        #     print("Координаты точки A -", ptA)
        #     print("Координаты точки B -", ptB)
        #     print("Продолжаю вычисления... (результаты могут быть некорректны)")
        line1 = Line(ptA, ptB)
        line2 = line1.perpendicular_line(ptC)
        ptE = line1.intersection(line2)
        c1 = Circle(ptE[0].evalf(), width)
        points1 = line2.intersection(c1)
        ptM = points1[0]
        c2 = Circle(ptE[0].evalf(), length)
        points2 = line1.intersection(c2)
        if ptA[0] > ptB[0]:
            ptK = points2[1]
        else:
            ptK = points2[0]
        line3 = line1.parallel_line(ptM)
        line4 = line2.parallel_line(ptK)
        box_angle = atan(line1.slope)
        if box_angle < 0:
            box_angle += pi
        ptH = line3.intersection(line4)
        ptG = Segment(ptE[0].evalf(), ptH[0].evalf()).midpoint
        if i == 0 or i == 2:
            return float(box_angle), float(ptG.x), float(ptG.y - width)
        if i == 1:
            return float(box_angle), float(ptG.x), float(ptG.y)

    def get_box_count(self):
        max_len = max([i["h1"] for i in self.coords_and_height])
        min_len = min([i["h1"] for i in self.coords_and_height])
        return int(((max_len - min_len) - self.pallet.BOX_TYPE["H"]) / self.pallet.BOX_TYPE["H"] + 1)


if __name__ == '__main__':
    robot = PortalRobot()
    robot.run()
    robot.get_pallet_info(3)
    robot.catch_boxes()
