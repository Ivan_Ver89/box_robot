import serial
import time
import matplotlib.pyplot as plt
import pwlf
import re


def get_border_points(coord, h):
    plt.plot(coord, h, "o")
    my_pwlf = pwlf.PiecewiseLinFit(coord, h)
    breaks = my_pwlf.fit(3)
    plt.show()
    return list(breaks)

# com-port робота
robot_port = serial.Serial()
robot_port.port = 'COM16'
robot_port.baudrate = 9600
# com-port дальномера
sensor_port = serial.Serial()
sensor_port.port = 'COM17'
sensor_port.baudrate = 115200

pattern = "L:\s\d+\r\n"

coords_and_height = list()

if not robot_port.is_open:
    robot_port.open()
if not sensor_port.is_open:
    sensor_port.open()

F_xy = 3000     # Скорость
height = 1400   # Высота

start = f"G1 X350 Y400 Z{height} F{F_xy}"
finish = f"G1 X350 Y100 Z{height} F{F_xy}"

robot_port.write(f"{start}\nM400\n".encode('ascii'))

response = ""
while response != "M400ok":
    response = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')

finish_coords = (float(finish.split(" ")[1][1:]), float(finish.split(" ")[2][1:]))
current_coords = None

while current_coords != finish_coords:
    robot_port.write(f"M114.2\n".encode('ascii'))
    robot_line = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    sensor_line = sensor_port.read(sensor_port.in_waiting).decode('UTF-8')
    res = re.findall(pattern, sensor_line)
    if len(res) > 0:
        if float(res[-1].split(" ")[1]) < 500:
            coords_and_height.append({
                "x": float(robot_line.split(" ")[2][2:]),
                "y": float(robot_line.split(" ")[3][2:]),
                "h": float(res[-1].split(" ")[1]),
            })
            print(float(res[-1].split(" ")[1]), "|", time.time())
    current_coords = (
        round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))


h = [i["h"] for i in coords_and_height]
y = [i["y"] for i in coords_and_height]

get_border_points(coord=y, h=h)



