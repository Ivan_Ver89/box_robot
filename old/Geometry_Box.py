from sympy import *
from old.math import degrees,atan

# Задаем параметры ящика
width = input("Введите ширину ящика: ")
length = input("Введите длину ящика: ")

# Получаем три точки с датчика: две на первой стороне, одну на перпендикулярной ей
ptB = [513.0211,896.0386]
ptA = [112.9438,735.9509]
ptC = [17.1571,536.0381]

# Базовая проверка корректности полученных точек
if ptA[1] >= ptB[1]:
    print("Точка A должна располагаться ниже точки B (координаты получены некорректно).")
    print("Координаты точки A -", ptA)
    print("Координаты точки B -", ptB)
    print("Продолжаю вычисления... (результаты могут быть некорректны)")

# Создаем прямую, проходящую через точки A и B
line1 = Line(ptA,ptB)

# Создаем прямую, проходящую через точку C перпендикулярно line1
line2 = line1.perpendicular_line(ptC)

# В месте пересечения прямых line1 и line2 обозначаем точку E
ptE = line1.intersection(line2)

# Создаем окружность с центром в точке E и радиусом, равным ширине ящика
c1 = Circle(ptE[0].evalf(), width)

# Находим точку пересечения окружности c1 с прямой line2
points1 = line2.intersection(c1)
ptM = points1[0]

# Создаем окружность с центром в точке E и радиусом, равным длине ящика
c2 = Circle(ptE[0].evalf(), length)

# Находим точку пересечения окружности c2 с прямой line1
points2 = line1.intersection(c2)
if ptA[0] > ptB[0]:
    ptK = points2[1]
else:
    ptK = points2[0]

# Создаем прямые, параллельные line1 и line2, проходящие через точки M и K соответственно
line3 = line1.parallel_line(ptM)
line4 = line2.parallel_line(ptK)

# В точке пересечения прямых line3 и line4 обозначаем точку H
ptH = line3.intersection(line4)

# В качестве центра ящика берем середину отрезка EH
ptG = Segment(ptE[0].evalf(),ptH[0].evalf()).midpoint

# Угол относительно оси X
box_angle = atan(line1.slope)
if box_angle < 0:
    box_angle += pi

print("Координаты точки E -", ptE[0].evalf())
print("Координаты точки M -", ptM.evalf())
print("Координаты точки K -", ptK.evalf())
print("Координаты точки H -", ptH[0].evalf())
print("Координаты точки G -", ptG.evalf())
print("Угол наклона ящика -", degrees(box_angle), "° (",box_angle.evalf(), "рад ): +X = 0°, +Y = 90°, -X=180°.")