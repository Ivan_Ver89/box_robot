from sympy import *
import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt
import pwlf


def get_geometry():
    # Задаем параметры ящика
    width = 400
    length = 600

    # Получаем три точки с датчика: две на первой стороне, одну на перпендикулярной ей
    ptB = [513.0211, 896.0386]
    ptA = [112.9438, 735.9509]
    ptC = [17.1571, 536.0381]
    #
    # ptA = points[0]
    # ptB = points[1]
    # ptC = points[2]

    # Базовая проверка корректности полученных точек
    if ptA[1] >= ptB[1]:
        print("Точка A должна располагаться ниже точки B (координаты получены некорректно).")
        print("Координаты точки A -", ptA)
        print("Координаты точки B -", ptB)
        print("Продолжаю вычисления... (результаты могут быть некорректны)")

    # Создаем прямую, проходящую через точки A и B
    line1 = Line(ptA,ptB)

    # Создаем прямую, проходящую через точку C перпендикулярно line1
    line2 = line1.perpendicular_line(ptC)

    # В месте пересечения прямых line1 и line2 обозначаем точку E
    ptE = line1.intersection(line2)

    # Создаем окружность с центром в точке E и радиусом, равным ширине ящика
    c1 = Circle(ptE[0].evalf(), width)

    # Находим точку пересечения окружности c1 с прямой line2
    points1 = line2.intersection(c1)
    ptM = points1[0]

    # Создаем окружность с центром в точке E и радиусом, равным длине ящика
    c2 = Circle(ptE[0].evalf(), length)

    # Находим точку пересечения окружности c2 с прямой line1
    points2 = line1.intersection(c2)
    if ptA[0] > ptB[0]:
        ptK = points2[1]
    else:
        ptK = points2[0]

    # Создаем прямые, параллельные line1 и line2, проходящие через точки M и K соответственно
    line3 = line1.parallel_line(ptM)
    line4 = line2.parallel_line(ptK)

    # В точке пересечения прямых line3 и line4 обозначаем точку H
    ptH = line3.intersection(line4)

    # В качестве центра ящика берем середину отрезка EH
    ptG = Segment(ptE[0].evalf(),ptH[0].evalf()).midpoint



    print("Координаты точки E -", ptE[0].evalf())
    print("Координаты точки M -", ptM.evalf())
    print("Координаты точки K -", ptK.evalf())
    print("Координаты точки H -", ptH[0].evalf())
    print("Координаты точки G -", ptG.evalf())

    return float((ptE[0].evalf().x + ptH[0].evalf().x)/2), float((ptE[0].evalf().y + ptH[0].evalf().y)/2)


def piecewise_linear(x, x0, y0, k1, k2):
    return np.piecewise(x, [x < x0], [lambda x:k1*x + y0-k1*x0, lambda x:k2*x + y0-k2*x0])

def get_border_points(x, y):
    my_pwlf = pwlf.PiecewiseLinFit(x, y)
    breaks = my_pwlf.fit(3)
    x_hat = np.linspace(min(x), max(x), 100)
    # y_hat = my_pwlf.predict(x_hat)
    # y_point = my_pwlf.predict(list(breaks)[2])
    # plt.figure()
    # plt.plot(x, y, 'o')
    # plt.plot(x_hat, y_hat, '-')
    # plt.plot(list(breaks)[2], y_point, 'X', color=(0.9, 0.2, 0.9))
    # plt.plot()
    # plt.show()
    return list(breaks)


    # p, e = optimize.curve_fit(piecewise_linear, x, y)
    # xs = np.linspace(min(x), max(x), 10)
    # ys = piecewise_linear(xs, *p)
    # pnts = list()
    # pnts.append((xs[0], ys[0]))
    # for i in range(1, len(ys)):
    #     if ys[i] > (pnts[-1][1] + 15) or ys[i] < (pnts[-1][1] - 15):
    #         pnts.append((xs[i], ys[i]))
    # pnts.append((xs[-1], ys[-1]))
    # lines = list()
    # for i in range(1, len(pnts)):
    #     lines.append(Line(pnts[i - 1], pnts[i]))
    # cross_points = list()
    # for i in range(1, len(lines)+1):
    #     cross_points.append((
    #         float(lines[i - 1].intersection(lines[i])[0].x),
    #         float(lines[i - 1].intersection(lines[i])[0].y)
    #     ))
    # return cross_points[0]



get_geometry()