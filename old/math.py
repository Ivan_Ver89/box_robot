from scipy import optimize
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sympy import *

x = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ,11, 12, 13, 14, 15], dtype=float)
y = np.array([5, 7, 9, 11, 13, 15, 28.92, 42.81, 56.7, 70.59, 84.47, 98.36, 112.25, 126.14, 140.03])


def get_cross_points(xs, ys):
    pnts = list()
    pnts.append((xs[0], ys[0]))
    for i in range(1, len(ys)):
        if ys[i] > (pnts[-1][1] + 5) or ys[i] < (pnts[-1][1] - 5):
            pnts.append((xs[i], ys[i]))
    pnts.append((xs[-1], ys[-1]))
    lines = list()
    for i in range(1, len(pnts)):
        lines.append(Line(pnts[i-1], pnts[i]))
    cross_points = list()
    for i in range(1, len(lines)):
        cross_points.append((
            float(lines[i-1].intersection(lines[i])[0].x),
            float(lines[i-1].intersection(lines[i])[0].y)
        ))
    pass


def piecewise_linear(x, x0, y0, k1, k2):
    res = np.piecewise(x, [x < x0], [lambda x:k1*x + y0-k1*x0, lambda x:k2*x + y0-k2*x0])
    return res


def get_lines(x, y):
    p, e = optimize.curve_fit(piecewise_linear, x, y)
    xd = np.linspace(min(x), max(x), 10)
    plt.plot(x, y, "o")
    yd = piecewise_linear(xd, *p)
    plt.plot(xd, yd)
    return get_cross_points(xd, yd)


df = pd.read_csv('data.csv', sep=",")
get_lines(x=[int(i) for i in df["A"]], y=[int(i) for i in df["B"]])
# get_lines(x=x, y=y)