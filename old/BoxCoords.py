import time

import pwlf
import serial
from sympy import *
from threading import Thread
import matplotlib.pyplot as plt
import logging


class BoxRobot:
    coords_and_height = list()
    F_up = 30000    # Скорость вверх
    F_down = 60000  # Скорость вниз
    F_xy = 120000    # Скорость по XY

    H_vacuum = 1000 # Высота присоски

    def __init__(self):
        # com-port робота
        self.robot_port = serial.Serial()
        self.robot_port.port = 'COM6'
        self.robot_port.baudrate = 9600
        # # com-port датчиков
        self.range_finder_port = serial.Serial()
        self.range_finder_port.port = 'COM1'
        self.range_finder_port.baudrate = 115200
        # # com-port панели управления

    def combine_data(self, j_code):
        self.coords_and_height.clear()
        finish_coords = (float(j_code.split(" ")[1][1:]), float(j_code.split(" ")[2][1:]))
        self.robot_port.write(f"{j_code}\n".encode('ascii'))
        current_coords = None
        while current_coords != finish_coords:
            self.robot_port.write(f"M114.2\n".encode('ascii'))
            robot_line = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            sensor_line = self.range_finder_port.read(self.range_finder_port.inWaiting()).decode('ascii')
            # box_is_find =
            try:
                heights = sensor_line.split("\r\n")[-2].split(" ")
                if 8191 not in [float(i) for i in heights]:
                    self.coords_and_height.append({
                        "x1": float(robot_line.split(" ")[2][2:]) + 50,
                        "y1": float(robot_line.split(" ")[3][2:]) - 100,
                        "h1": float(heights[1]),
                    })
                    current_coords = (
                        round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))
                time.sleep(0.02)
            except:
                pass