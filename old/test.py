import operator

column = [
         {"id":2, "h": None, "count": 2, "x": 930, "y": 630},
         {"id":3, "h": None, "count": 4, "x": 330, "y": 630},
         {"id":4, "h": None, "count": 2, "x": 330, "y": 230},
         {"id":1, "h": None, "count": 4, "x": 930, "y": 230},
    ]

sorted_columns = sorted(column, key=lambda x: (-x["count"], x["id"]))

print(sorted_columns)