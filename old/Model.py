import time
from old.math import degrees

import matplotlib.pyplot as plt
import serial
from sympy import *
import re

# максимальные положения робота
#  ось X -1390 со старой головой
#  ось Y - 1780 со старой головой
#  ось Z -

#положение паллета
# ЛН - G1 X 30 Y 30
# ЛВ - G1 X 30 Y 830
# ВП - G1 X 1230 Y 830
# ПН - G1 X 1230 Y 30

BOXES_TYPES = {
    1: {"L": 600, "W": 400, "H": 115, "delta": 115, "inner": True, "perimeter": True, "blow": False},
    2: {"L": 600, "W": 400, "H": 118, "delta": 188, "inner": True, "perimeter": True, "blow": False},
    3: {"L": 600, "W": 400, "H": 183, "delta": 183, "inner": False, "perimeter": True, "blow": False},
    4: {"L": 600, "W": 400, "H": 110, "delta": 110, "inner": False, "perimeter": True, "blow": False},
    5: {"L": 600, "W": 400, "H": 188, "delta": 188, "inner": True, "perimeter": True, "blow": False},
    6: {"L": 540, "W": 340, "H": 298, "delta": 80, "inner": True, "perimeter": False, "blow": True},
    7: {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "perimeter": False, "blow": False},
    8: {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "perimeter": False, "blow": False},
}


class Pallet:
    BOX_TYPE = None
    pallet_h = 0
    info_boxes = [
        {"number": None, "angle": None, "coords": [None, None]},
        {"number": None, "angle": None, "coords": [None, None]},
        {"number": None, "angle": None, "coords": [None, None]},
        {"number": None, "angle": None, "coords": [None, None]},
    ]

    def __init__(self, box_type):
        self.BOX_TYPE = BOXES_TYPES[box_type]

    def set_box_info(self, n, number, *args):
        self.info_boxes[n]["number"] = number
        self.info_boxes[n]["angle"] = args[0]
        self.info_boxes[n]["coords"] = [args[1], args[2]]

    def __str__(self):
        return str(self.info_boxes)


class PortalRobot:
    border_coords = list()
    is_moving = false
    pallet = None
    coords_and_height = list()
    total_number_of_boxes = 0
    max_height = 2000
    F_xy = 20000  # Скорость по XY
    pattern = "L:\s\d{3,}"
    homing = ["M81", "G28 X F6000", "G28 Y F6000", "G28 Z F6000"]
    z=2000

    def __init__(self):
        self.set_g_code()
        # com-port робота
        self.robot_port = serial.Serial()
        self.robot_port.port = 'COM16'
        self.robot_port.baudrate = 9600
        # com-port дачика
        self.sensor_port = serial.Serial()
        self.sensor_port.port = 'COM17'
        self.sensor_port.baudrate = 115200
        # # com-port панели
        # self.panel_port = serial.Serial()
        # self.panel_port.port = 'COM1'
        # self.panel_port.baudrate = 115200
        ######################################
        if not self.robot_port.is_open:
            self.robot_port.open()
        if not self.sensor_port.is_open:
            self.sensor_port.open()
        # if not self.panel_port.is_open:
        #     self.panel_port.open()
        # if self.panel_port.is_open:
        #     Thread(target=self.listen_buttons).start()

    def set_g_code(self):
        # self.to_start = f"G1 X675 Y1100 F{self.F_xy}"
        # self.to_start = f"G1 X518 Y1020 F{self.F_xy}\nM400"
        self.to_start = f"G1 X820 Y1000 Z{self.z} F20000\nG4P500\nM400" #старт
        self.scan_g_code = [

            # # ЯЩИК 1
            # f"G1 X780 Y1000 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №1 к мойке
            # f"G1 X780 Y850 Z{self.z} F30000",  # окончание сканирования
            #
            # f"G1 X1070 Y1000 Z{self.z} F10000\nG4P1000\nM400",  # ТОЧКА №2
            # f"G1 X1070 Y850 Z{self.z} F30000",  # окончание сканирования
            #
            # f"G1 X1280 Y700 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №3
            # f"G1 X1180 Y700 Z{self.z} F10000"  # окончание сканирования


            # # ЯЩИК 2
            f"G1 X520 Y1000 Z{self.z} F60000\nG4P500\nM400", # ТОЧКА №1 к мойке
            f"G1 X520 Y850 Z{self.z} F30000",  # окончание сканирования

            f"G1 X230 Y1000 Z{self.z} F10000\nG4P1000\nM400", # ТОЧКА №2
            f"G1 X230 Y850 Z{self.z} F30000", # окончание сканирования

            f"G1 X20 Y700 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №3
            f"G1 X120 Y700 Z{self.z} F10000"  # окончание сканирования

            # # # ЯЩИК 3
            # f"G1 X520 Y68 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №1 к мойке
            # f"G1 X520 Y218 Z{self.z} F30000",  # окончание сканирования
            #
            # f"G1 X230 Y68 Z{self.z} F10000\nG4P1000\nM400",  # ТОЧКА №2
            # f"G1 X230 Y218 Z{self.z} F30000",  # окончание сканирования
            #
            # f"G1 X20 Y160 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №3
            # f"G1 X120 Y160 Z{self.z} F10000"  # окончание сканирования

            # ЯЩИК 4
            # f"G1 X780 Y68 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №1 к мойке
            # f"G1 X780 Y218 Z{self.z} F30000",  # окончание сканирования
            #
            # f"G1 X1070 Y68 Z{self.z} F10000\nG4P1000\nM400",  # ТОЧКА №2
            # f"G1 X1070 Y218 Z{self.z} F30000",  # окончание сканирования
            #
            # f"G1 X1280 Y160 Z{self.z} F60000\nG4P500\nM400",  # ТОЧКА №3
            # f"G1 X1180 Y160 Z{self.z} F10000"  # окончание сканирования
            #



            # [f"G1 X318 Y860 F{self.F_xy}", f"G1 X0 Y860 F{self.F_xy}", f"G1 X318 Y631 F{self.F_xy}"],
            # [f"G1 X0 Y343 F{self.F_xy}", f"G1 X318 Y260 F{self.F_xy}", f"G1 X551 Y63 F{self.F_xy}"],
            # [f"G1 X1031 Y260 F{self.F_xy}", f"G1 X1350 Y343 F{self.F_xy}", f"G1 X1031 Y485 F{self.F_xy}"],
            # [f"G1 X1350 Y718 F{self.F_xy}", f"G1 X1031 Y860 F{self.F_xy}", f"G1 X675 Y1020 F{self.F_xy}"],
        ]
        self.next_g_code = [
            # f"G1 X318 Y1020 F{self.F_xy}\nM400",
            # f"G1 X518 Y1020 F{self.F_xy}\nM400",

            # f"G1 X318 Y485 F{self.F_xy}\nM400",
            # f"G1 X789 Y63 F{self.F_xy}\nM400",
            # f"G1 X1031 Y631 F{self.F_xy}\nM400"
        ]

    def run(self):
        # for g_code in self.homing:
        #     self.move(g_code)
        # Начало
        self.move(self.to_start) # Холостой ход
        self.get_pallet_info(2) # Сканирование


    # def listen_buttons(self):
    #     while True:
    #         response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    #         if response and not self.is_moving:
    #             # sdgsdfgdfg
    #             self.is_moving = true
    #             self.box = BOXES_TYPES[response]

    def move(self, g_code):
        self.is_moving = True
        self.robot_port.write(f"{g_code}\nM400\n".encode('ascii'))
        response = ""
        while response != "M400ok":
            # print(response)
            response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            line = self.sensor_port.read(self.sensor_port.in_waiting)
        else:
            self.is_moving = False
        line = self.robot_port.read(self.robot_port.in_waiting)

    # Сканирование
    def get_pallet_info(self, box_number):
        self.pallet = Pallet(box_type=box_number)
        # проходится по списку г-кода
        for g_code in self.scan_g_code:
            # print(g_code)
            if "M400" in g_code:
                self.move(g_code) # Холостой ход
            else:
                self.move_and_scan(g_code) # Сканирование
        res = self.get_geometry(width=400, length=600, points=self.border_coords)
        if res:
            r = list(res)
            # корректировака
            r[1] -= 20
            r[2] -= 104
            print("Центр ящика-", r) #вывод центра ящика
            self.move(f"G1 X{r[1]} Y{r[2]}") # Движение в кординаты центра коробки




        # for i, i_code in enumerate(self.next_g_code):
        #     self.move(i_code)
        #     for j, j_code in enumerate(self.scan_g_code[i]):
        #         self.move_and_scan(j_code, i=i, j=j)
        #     self.pallet.set_box_info(i, 1, *self.get_geometry(length=self.pallet.BOX_TYPE["L"],
        #                                                       width=self.pallet.BOX_TYPE["W"],
        #                                                       points=self.border_coords, i=i))
        #
        # print(self.pallet)

    def move_and_scan(self, g_code):
        self.combine_data(g_code)
        self.coords_and_height[0]['delta'] = 0
        self.coords_and_height[1]['delta'] = 0
        for i in range(2, len(self.coords_and_height)):
            try:
                self.coords_and_height[i]['delta'] = abs(
                    self.coords_and_height[i]["h"] - self.coords_and_height[i - 2]["h"]) #расчет дельты
                # print(self.coords_and_height[i]["x"], self.coords_and_height[i]["y"], self.coords_and_height[i]["h"])
            except:
                self.coords_and_height[i]['delta'] = 0

        # f = max(self.coords_and_height, key=lambda x: max(x["delta"])) # значение точки с Макс дельтаой
        mmin = self.coords_and_height[0]
        for i in self.coords_and_height:
            try:
                if i["h"] < mmin["h"]:
                    mmin = i
            except:
                mmin = mmin
        self.border_coords.append((mmin['x'], mmin['y']))
        print(f"G1 X{mmin['x']} Y{mmin['y']}") # Вывод в консоль координат и высоты найденной точки на границе коробки

        # if (i, j) == (0, 0) or (i, j) == (1, 1) or (i, j) == (2, 0) or (i, j) == (2, 2) or (i, j) == (3, 1):
        #     self.border_coords.append((self.get_border_points([k["x"] for k in self.coords_and_height],
        #                                                  [k["h"] for k in self.coords_and_height])[2],
        #
        #                           self.get_border_points([k["y"] for k in self.coords_and_height],
        #                                                  [k["h"] for k in self.coords_and_height])[2]
        #                           ))
        # else:
        #     self.border_coords.append((self.get_border_points([k["x"] for k in self.coords_and_height],
        #                                                  [k["h"] for k in self.coords_and_height])[1],
        #
        #                           self.get_border_points([k["y"] for k in self.coords_and_height],
        #                                                  [k["h"] for k in self.coords_and_height])[1]
        #                           ))

    # def catch_boxes(self):
    #     print(self.pallet)
    #     while self.pallet.info_boxes[0]["number"] != 0 or self.pallet.info_boxes[1]["number"] != 0 or \
    #             self.pallet.info_boxes[2]["number"] != 0 or self.pallet.info_boxes[3]["number"] != 0:
    #         max_l = max(self.pallet.info_boxes, key=lambda b: b["number"])
    #         min_l = min(self.pallet.info_boxes, key=lambda b: b["number"])
    #         if max_l == min_l:
    #             current_g_code = self.send_to_check(
    #                 x=self.pallet.info_boxes[0]["coords"][0],
    #                 y=self.pallet.info_boxes[0]["coords"][1],
    #                 z=(self.max_hight - self.pallet.info_boxes[0]["number"] * BOXES_TYPES[self.pallet.BOX_TYPE][
    #                     "delta"] - BOXES_TYPES[self.pallet.BOX_TYPE]["H"]),
    #                 alpha=self.pallet.info_boxes[0]["coords"][0],
    #                 box_type=self.pallet.BOX_TYPE)
    #             self.robot_port.write(f"{current_g_code}\n".encode('ascii'))
    #             print(f'Отправка на com порт {current_g_code}')
    #             self.pallet.info_boxes[0]["number"] -= 1
    #             response = ""
    #             while response != "M400ok":
    #                 response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    #         else:
    #             current_g_code = self.send_to_check(
    #                 x=max_l["coords"][0],
    #                 y=max_l["coords"][1],
    #                 z=(self.max_hight - max_l["number"] * BOXES_TYPES[self.pallet.BOX_TYPE]["delta"] -
    #                    BOXES_TYPES[self.pallet.BOX_TYPE]["H"]),
    #                 alpha=max_l["coords"][0],
    #                 box_type=self.pallet.BOX_TYPE)
    #             self.robot_port.write(f"{current_g_code}\n".encode('ascii'))
    #             print(f'Отправка на com порт {current_g_code}')
    #             max_l["number"] -= 1
    #             response = ""
    #             while response != "M400ok":
    #                 response = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    #         print(self.pallet)
    #     print(self.pallet)

    def combine_data(self, j_code):
        # sensor_line = self.sensor_port.read(self.sensor_port.in_waiting).decode('UTF-8').replace("\n", "").replace("\r", "")
        # sensor_line =""

        self.robot_port.flush()
        self.robot_port.flushInput()
        self.robot_port.flushOutput()

        self.coords_and_height.clear()
        finish_coords = (float(j_code.split(" ")[1][1:]), float(j_code.split(" ")[2][1:]))

        self.robot_port.write(f"{j_code}\n".encode('ascii'))
        ll = self.robot_port.read(self.robot_port.in_waiting)
        ll = self.robot_port.read(self.robot_port.in_waiting)
        ll = self.robot_port.read(self.robot_port.in_waiting)
        ll = self.robot_port.read(self.robot_port.in_waiting)

        self.robot_port.flush()
        self.robot_port.flushInput()
        self.robot_port.flushOutput()

        current_coords = None
        while current_coords != finish_coords:
            self.robot_port.write(f"M114.2\n".encode('ascii'))
            robot_line = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            sensor_line = self.sensor_port.read(self.sensor_port.in_waiting)

            try:

                sensor_line = sensor_line.decode('UTF-8')
                # sensor_line = "L: 365\r\n"
                res = re.findall(self.pattern, sensor_line)
                if len(res) > 0:
                    if float(res[-1].split(" ")[1]) < 3000:
                        self.coords_and_height.append({
                            "x": float(robot_line.split(" ")[2][2:]),
                            "y": float(robot_line.split(" ")[3][2:]),
                            "h": float(res[0].split(" ")[1]),
                            # "time": time.time(),
                            # "delta_time": time.time() - start_time
                        })
                        print(
                            float(robot_line.split(" ")[2][2:]),
                            float(robot_line.split(" ")[3][2:]),
                            # sensor_line,
                            float(res[-1].split(" ")[1])
                            # time.time(),
                            # time.time() - start_time
                        )
                    else:
                        self.coords_and_height.append({
                            "x": float(robot_line.split(" ")[2][2:]),
                            "y": float(robot_line.split(" ")[3][2:]),
                            "h": "error",
                            # "time": time.time(),
                            # "delta_time": time.time() - start_time
                        })
                        print(
                            float(robot_line.split(" ")[2][2:]),
                            float(robot_line.split(" ")[3][2:]),
                            # sensor_line,
                            "error"
                            # time.time(),
                            # time.time() - start_time
                        )
                else:
                    # pass
                    print(
                        float(robot_line.split(" ")[2][2:]),
                        float(robot_line.split(" ")[3][2:]),
                        # sensor_line,
                        "error"
                        # time.time(),
                        # time.time() - start_time
                    )
                current_coords = (
                round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))
            except Exception:
                pass
                # if robot_line != "ok":
                #     print(
                #         float(robot_line.split(" ")[2][2:]),
                #         float(robot_line.split(" ")[3][2:]),
                #         # sensor_line,
                #         "error"
                #         # time.time(),
                #         # time.time() - start_time
                #     )
            time.sleep(0.01)

    # @staticmethod
    # def get_border_points(coord, h):
    #     plt.plot(coord, h, "o")
    #     my_pwlf = pwlf.PiecewiseLinFit(coord, h)
    #     breaks = my_pwlf.fit(3)
    #     # plt.show()
    #     return list(breaks)

    @staticmethod
    def get_geometry(width, length, points):
        # Получаем три точки с датчика: две на первой стороне, одну на перпендикулярной ей
        ptA = points[0]
        ptB = points[1]
        ptC = points[2]
        # print("Введены следующие координаты исходных точек:")
        # print("Точка A: ", ptA)
        # print("Точка B: ", ptB)
        # print("Точка C: ", ptC)

        # Создаем прямую AB, проходящую через точки A и B
        line1 = Line(ptA, ptB)

        # Создаем прямую CE, проходящую через точку C перпендикулярно line1
        line2 = line1.perpendicular_line(ptC)

        # В месте пересечения прямых line1 и line2 обозначаем точку E
        ptE = line1.intersection(line2)
        print("Найдены следующие координаты угловых точек:")
        print("Координаты точки ЛВ E -", ptE[0].evalf())

        # Проводим луч из точки E в направлении точки C
        rayEC = Ray(ptE[0], ptC)

        # Создаем окружность с центром в точке E и радиусом, равным ширине ящика
        c1 = Circle(ptE[0], width)

        # Находим точку M пересечения окружности c1 с лучом rayEC
        # points1 = line2.intersection(c1)
        # ptM = points1
        ptM = rayEC.intersection(c1)
        print("Координаты точки ЛН M -", ptM[0].evalf())

        # Создаем окружность с центром в точке E и радиусом, равным длине ящика
        c2 = Circle(ptE[0], length)

        # Проводим луч из точки E в направлении точки A
        rayEA = Ray(ptE[0], ptA)

        # Находим точку K пересечения окружности c2 с лучом rayEA
        ptK = rayEA.intersection(c2)
        print("Координаты точки ПВ K -", ptK[0].evalf())

        # Создаем прямые, параллельные line1 и line2, проходящие через точки M и K соответственно
        line3 = line1.parallel_line(ptM[0])
        line4 = line2.parallel_line(ptK[0])

        # В точке пересечения прямых line3 и line4 обозначаем точку H
        ptH = line3.intersection(line4)
        print("Координаты точки ПН H -", ptH[0].evalf())

        # В качестве центра ящика берем середину отрезка EH
        ptG = Segment(ptE[0].evalf(), ptH[0].evalf()).midpoint
        print("Координата центра ящика G -", ptG.evalf())

        # Угол относительно оси X
        box_angle = atan(line1.slope)
        if box_angle < 0:
            box_angle += pi

        print("Угол наклона ящика -", degrees(box_angle), "° (", box_angle, "рад )*")
        print(
            "* Отсчет угла ведется от положительного направления оси X против часовой стрелки: +X = 0°, +Y = 90°, -X=180°.")

        plt.scatter(x=float(ptE[0].x), y=float(ptE[0].y), c="green", edgecolors="green")
        plt.plot([float(ptE[0].x), float(ptM[0].x)], [float(ptE[0].y), float(ptM[0].y)], c="green")

        plt.scatter(x=float(ptM[0].x), y=float(ptM[0].y), c="green", edgecolors="green", alpha=0.6)
        plt.plot([float(ptM[0].x), float(ptH[0].x)], [float(ptM[0].y), float(ptH[0].y)], c="green")

        plt.scatter(x=float(ptH[0].x), y=float(ptH[0].y), c="green", edgecolors="green", alpha=0.6)
        plt.plot([float(ptH[0].x), float(ptK[0].x), ], [float(ptH[0].y), float((ptK[0].y)), ], c="green")

        plt.scatter(x=float(ptK[0].x), y=float(ptK[0].y), c="green", edgecolors="green", alpha=0.6)
        plt.plot([float(ptK[0].x), float(ptE[0].x)], [float(ptK[0].y), float(ptE[0].y)], c="green")

        plt.scatter(x=float(ptA[0]), y=float(ptA[1]), c="red", edgecolors="red")
        plt.scatter(x=float(ptB[0]), y=float(ptB[1]), c="red", edgecolors="red")
        plt.scatter(x=float(ptC[0]), y=float(ptC[1]), c="red", edgecolors="red")

        plt.scatter(x=float(ptG.x), y=float(ptG.y), c="red", edgecolors="blue")
        # plt.scatter(ptM, 'O')
        # plt.scatter(ptK, 'O')
        # plt.scatter(ptH, 'O')
        plt.show()

        return float(degrees(box_angle)), float(ptG.x), float(ptG.y)


    # def get_box_count(self):
    #     max_len = max([i["h2"] for i in self.coords_and_height])
    #     min_len = min([i["h2"] for i in self.coords_and_height])
    #     return int(((max_len - min_len) - self.pallet.BOX_TYPE["H"]) / self.pallet.BOX_TYPE["H"] + 1)


if __name__ == '__main__':
    robot = PortalRobot()
    robot.run()
