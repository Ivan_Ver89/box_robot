import serial
import time
import matplotlib.pyplot as plt
import pwlf
import  re

pattern = "L:\s\d+\r\n"

def get_border_points(coord, h):
    plt.plot(coord, h, "o")
    my_pwlf = pwlf.PiecewiseLinFit(coord, h)
    breaks = my_pwlf.fit(3)
    plt.show()
    # return list(breaks)

# com-port робота
robot_port = serial.Serial()
robot_port.port = 'COM16'
robot_port.baudrate = 9600
# com-port дальномера
sensor_port = serial.Serial()
sensor_port.port = 'COM17'
sensor_port.baudrate = 115200

coords_and_height = list()

# if not robot_port.is_open:
#     robot_port.open()
if not sensor_port.is_open:
    sensor_port.open()

F_xy = 3000     # Скорость
height = 1500   # Высота

# start = f"G1 X350 Y1100 Z{height} F{F_xy}"
# finish = f"G1 X350 Y200 Z{height} F{F_xy}"

# robot_port.write(f"{start}\nM400\n".encode('ascii'))

response = ""
# while response != "M400ok":
#     response = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')

# finish_coords = (float(finish.split(" ")[1][1:]), float(finish.split(" ")[2][1:]))
current_coords = None

# import serial
# serial_port1 = serial.Serial('COM15', 115200)



# while(True):
#     print(serial_port1.is_open)
#     serial_port1.read(serial_port1.in_waiting)
#     print(serial_port1.in_waiting)
import matplotlib.pyplot as plt

t = list()
h = list()
s = time.time()
while time.time() < s + 5:
# while current_coords != finish_coords:
    # robot_port.write(f"M114.2\n".encode('ascii'))
    # robot_line = robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
    # sensor_line = sensor_port.readline().decode('UTF-8').replace('\n', '').replace('\r', '')

    # print(sensor_port.is_open)
    try:
        sensor_line = sensor_port.read(sensor_port.in_waiting).decode('UTF-8')
        res = re.findall(pattern, sensor_line)
        if len(res) > 0:
            if float(res[-1].split(" ")[1]) < 500:
                h.append(float(res[-1].split(" ")[1]))
                t.append(time.time())
                print(float(res[-1].split(" ")[1]), "|", time.time())
    except:
        pass
    # time.sleep(0.009)
#     try:
#         if "L" in sensor_line:
#             coords_and_height.append({
#                 # "x": float(robot_line.split(" ")[2][2:]),
#                 # "y": float(robot_line.split(" ")[3][2:]),
#                 # "h": float(sensor_line.split(" ")[-1][0:-5]),
#             })
#             # current_coords = (round(float(robot_line.split(" ")[2][2:])), round(float(robot_line.split(" ")[3][2:])))
#         # time.sleep(0.0002)
#     except:
#         pass
#
# h = [i["h"] for i in coords_and_height]
# y = [i["y"] for i in coords_and_height]
#
# get_border_points(coord=y, h=h)
plt.plot(t, h, "o")
plt.show()



