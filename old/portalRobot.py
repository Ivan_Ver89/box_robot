import logging
import time

import serial
from old.Box_Geometry import get_border_points, get_geometry


boxes_type = {
    1: {"L": 600, "W": 400, "H": 115, "delta": 115, "inner": True, "outer": True, "blow": False},
    2: {"L": 600, "W": 400, "H": 118, "delta": 188, "inner": True, "outer": True, "blow": False},
    3: {"L": 600, "W": 400, "H": 183, "delta": 183, "inner": False, "outer": True, "blow": False},
    4: {"L": 600, "W": 400, "H": 110, "delta": 110, "inner": False, "outer": True, "blow": False},
    5: {"L": 600, "W": 400, "H": 188, "delta": 188, "inner": True, "outer": True, "blow": False},
    6: {"L": 540, "W": 340, "H": 298, "delta": 80, "inner": True, "outer": False, "blow": True},
    7: {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "outer": False, "blow": False},
    8: {"L": 530, "W": 340, "H": 202, "delta": 58, "inner": True, "outer": False, "blow": False},
}


class PortalRobot_:
    def __init__(self):
        #com-port робота
        self.box_type = None
        self.robot_port = serial.Serial()
        self.robot_port.port = 'COM11'
        self.robot_port.baudrate = 9600
        # com-port дальномера
        self.range_finder_port = serial.Serial()
        self.range_finder_port.port = 'COM4'
        self.range_finder_port.baudrate = 4800

        self.coordsAndHeight = list()
        self.border_points = list()


    def run(self):
        if not self.robot_port.is_open:
            self.robot_port.open()
        if not self.range_finder_port.is_open:
            self.range_finder_port.open()
        logging.warning(f"robot port open - {self.robot_port.is_open}")
        logging.warning(f"range finder port open - {self.range_finder_port.is_open}")

    def send_coordinates(self, g_code_list, number):
        self.number = number
        self.robot_port.write(f"{g_code_list[0]}\n".encode('ascii'))
        response = ""
        while "ok" not in response:
            if self.robot_port.in_waiting >= 1:
                line = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
                if len(line) > 0:
                    response = line

        self.collect_coordinates_y({"x": float(g_code_list[0].split(" ")[1][1:]),
                                    "y": float(g_code_list[0].split(" ")[2][1:])})

        self.robot_port.write(f"{g_code_list[1]}\n".encode('ascii'))
            # Thread(target=self.collect_coordinates,
            #    args=({"x": float(code.split(" ")[1][1:]),
            #           "y": float(code.split(" ")[2][1:])},)).run()

        self.collect_coordinates_x({"x": float(g_code_list[1].split(" ")[1][1:]),
                                    "y": float(g_code_list[1].split(" ")[2][1:])})

        print(self.border_points)

        X, Y = get_geometry(width=boxes_type[self.number]['W'], length=boxes_type[self.number]['L'], points=self.border_points)

        print(float(X), float(Y))

        self.robot_port.write(f"G1 X{float(X)} Y{float(Y)}\n".encode('ascii'))



    def collect_coordinates_y(self, finish_coord):
        current_coord = {"x": None, "y": None}
        while finish_coord["x"] != current_coord["x"] and finish_coord["y"] != current_coord["y"]:
            self.robot_port.write(f"M114.2\n".encode('ascii'))
            coords = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')

            try:
                heights = self.range_finder_port.read(self.range_finder_port.inWaiting()).decode('ascii').split("\r\n")[-2].split(" ")
                # print(heights)
                if 8191 not in [float(i) for i in heights]:
                    self.coordsAndHeight.append({
                        "x": float(coords.split(" ")[2][2:]),
                        "y": float(coords.split(" ")[3][2:]),
                        "h1": float(heights[1]),
                        "h2": float(heights[2]),
                    })
                    current_coord["x"] = float(coords.split(" ")[2][2:])
                    current_coord["y"] = float(coords.split(" ")[3][2:])
                time.sleep(0.02)
            except:
                pass


        self.get_box_count()
        y_border1 = get_border_points(x=[i['y'] for i in self.coordsAndHeight], y=[i['h2'] for i in self.coordsAndHeight])
        y_border2 = get_border_points(x=[i['y'] for i in self.coordsAndHeight], y=[i['h1'] for i in self.coordsAndHeight])
        p1 = [self.coordsAndHeight[2]["x"], y_border1[2]]
        p2 = [self.coordsAndHeight[2]["x"] + 300, y_border2[2]]

        self.border_points.append(p1)
        self.border_points.append(p2)

        print("go to border")
        # self.robot_port.write(f"G1 X{point}\n".encode('ascii'))

    def collect_coordinates_x(self, finish_coord):
        self.coordsAndHeight.clear()
        current_coord = {"x": None, "y": None}
        while finish_coord["x"] != current_coord["x"]:
            self.robot_port.write(f"M114.2\n".encode('ascii'))
            coords = self.robot_port.readline().decode('ascii').replace('\n', '').replace('\r', '')
            try:
                heights = \
                self.range_finder_port.read(self.range_finder_port.inWaiting()).decode('ascii').split("\r\n")[-2].split(" ")
                if 8191 not in [float(i) for i in heights]:
                    self.coordsAndHeight.append({
                        "x": float(coords.split(" ")[2][2:]),
                        "y": float(coords.split(" ")[3][2:]),
                        "h1": float(heights[1]),
                        "h2": float(heights[2]),
                    })
                    current_coord["x"] = float(coords.split(" ")[2][2:])
                    current_coord["y"] = float(coords.split(" ")[3][2:])
                time.sleep(0.02)
            except:
                pass

        self.get_box_count()
        y_border1 = get_border_points(x=[i['x'] for i in self.coordsAndHeight],
                                      y=[i['h1'] for i in self.coordsAndHeight])

        p1 = [self.coordsAndHeight[3]["x"], y_border1[3]]

        self.border_points.append(p1)

        print("go to border")


    def get_box_count(self):
        max_len = max([i["h2"] for i in self.coordsAndHeight])
        min_len = min([i["h2"] for i in self.coordsAndHeight])
        count = int(((max_len - min_len) - boxes_type[self.number]['H'])/boxes_type[self.number]['delta']+1)
        print(count)


    def get_box_borders(self):
        pass


