import asyncio
import logging
import time

import serial

import exceptions


class Robot:
    online = True
    _moving_ = False
    start_X = 695
    start_Y = 1130
    F_xy = 120000
    # F_xy = 20000
    F_down = 120000
    # F_down = 50000
    F_up = 120000
    # F_up = 20000
    first_time_box_output = 6
    t0 = 0
    start_time = 0
    box_in_washing_machine = False

    def run(self, controller):
        self.controller = controller

    def ping_port(self):
        self.controller.send_to_robot_port("M114.2")
        self.controller.read_from_robot_port()

    async def sleep(self):
        await asyncio.sleep(0.005)
        self.controller.to_home_status()
        await asyncio.sleep(0.005)
        self.go_to_start()
        await asyncio.sleep(0.005)
        named_tuple = time.localtime()
        time_string = time.strftime("%H:%M:%S", named_tuple)
        print("ОТМЕТКА ВРЕМЕНИ 2 sleep: ", time_string)
        self.move("G1 Z100 F10000")
        await asyncio.sleep(0.005)
        self.controller.sleep_status()



    async def change_of_gripper(self):
        self.controller.to_home_status()
        named_tuple = time.localtime()
        time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
        print("ОТМЕТКА ВРЕМЕНИ 3 гриппер: ", time_string)
        await asyncio.sleep(0.005)
        self.move_without_M400("M40.3")
        await asyncio.sleep(0.005)
        self.move_without_M400("M40.4")
        await asyncio.sleep(0.005)
        self.go_to_start()
        await asyncio.sleep(0.005)
        self.move("G1 Z1200")
        await asyncio.sleep(0.005)
        self.controller.wait_status()


    async def homing(self):
        await asyncio.sleep(0.005)
        self.controller.to_home_status()
        await asyncio.sleep(0.005)
        # for code in ["M81", "G28 A", "G28 Z F15000","G28 X F15000", "G28 Y F15000"]:
        self.move_without_M400("M40.3")
        await asyncio.sleep(0.005)
        self.move_without_M400("M40.4")
        await asyncio.sleep(0.005)
        self.move_without_M400("M40.1")
        await asyncio.sleep(0.005)
        self.move_without_M400("M40.2")
        await asyncio.sleep(0.005)
        self.move_without_M400("M81")
        await asyncio.sleep(0.005)
        self.move_without_M400("G4P1000")
        await asyncio.sleep(0.005)
        # time.sleep(2)
        named_tuple = time.localtime()
        time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
        print("ОТМЕТКА ВРЕМЕНИ 4 хоуминг: ", time_string)
        for code in ["G28 Z", "G28 Y", "G1 Y400 F10000", "G28 A", "G1 A90 F10000", "G28 X", "G1 X400 F10000", "G1 A0"]:
            await asyncio.sleep(0.005)
            self.move(code)
        self.go_to_start()
        self.controller.wait_status()

    def go_to_start(self):
        self.move_without_M400(f"G1 X{self.start_X} Y{self.start_Y} F{self.F_xy}")

    def up_modifided_g_code(self, g_code):
        if 'F' not in g_code:
            g_code += f' F{self.F_up}'
        # return f'M203.1 Z600 {g_code} M203.1 Z600'
        return f'M203.1 Z500 {g_code} M203.1 Z500'

    def down_modifided_g_code(self, g_code):
        if 'F' not in g_code:
            g_code += f' F{self.F_down}'
        # return f'M203.1 Z1000 {g_code} M203.1 Z600'
        return f'M203.1 Z800 {g_code} M203.1 Z500'

    def scan_while_move(self, g_code, sensor):
        if "Z" in g_code:
            # Если текущие координаты больше целевых - то вверх, иначе вниз
            if self.get_current_coordinates()[2] != self.parse_g_code(g_code=g_code)['Z']:
                if self.get_current_coordinates()[2] > self.parse_g_code(g_code=g_code)['Z']:
                    # g_code += f" F{self.F_up}"
                    g_code = self.up_modifided_g_code(g_code=g_code)
                else:
                    # g_code += f" F{self.F_down}"
                    g_code = self.down_modifided_g_code(g_code=g_code)

        # self.move("M400")
        logging.info("start scan")
        # g_code += "\nM400"
        self.controller.send_to_robot_port(g_code)
        # response = ""
        # while response != "M400ok":
        #     is_hooked = sensor.box_is_hooked()
        #     print(f"is_hooked - {is_hooked}")
        #     if is_hooked == 404:
        #         continue
        #         # raise exceptions.SensorOfflineException
        #     else:
        #         if not is_hooked:  # Если ящик не зацеплен
        #             self.move_without_M400("M40.1")  # вЫключили присоски ПРОВЕРИТЬ ВЕРОЯТНО РАБОТАЕТ НЕ ПРАВИЛЬНО
        #             self.controller.box_fell_down_error()
        #             while True:
        #                 try:
        #                     self.controller.send_to_robot_port("M114.2")
        #                 except serial.serialutil.SerialException:
        #                     raise exceptions.RobotOfflineException
        #     response = self.controller.read_from_robot_port()
        # else:
        #     self.is_moving = False

    def move(self, g_code):
        self.is_moving = True
        # Проверка здесь
        if "G1" in g_code:
            if "M400" not in g_code:
                if "Z" in g_code:
                    # Если текущие координаты больше целевых - то вверх, иначе вниз
                    if self.get_current_coordinates()[2] != self.parse_g_code(g_code=g_code)['Z']:
                        if self.get_current_coordinates()[2] > self.parse_g_code(g_code=g_code)['Z']:
                            # g_code += f" F{self.F_up}"
                            g_code = self.up_modifided_g_code(g_code=g_code)
                        else:
                            # g_code += f" F{self.F_down}"
                            g_code = self.down_modifided_g_code(g_code=g_code)
                g_code += "\nM400"
        if "G28" in g_code:
            g_code += "\nM400"

        try:
            self.controller.send_to_robot_port(g_code)
            response = ""
            while response != "M400ok":
                response = self.controller.read_from_robot_port()
            else:
                self.is_moving = False
        except serial.serialutil.SerialException:
            raise exceptions.RobotOfflineException

    def move_without_M400(self, g_code):
        # Проверка здесь
        if "G1" in g_code:
            if "Z" in g_code:
                # print()
                if "F" not in g_code:
                    # Если текущие координаты больше целевых - то вверх, иначе вниз
                    # print("self.get_current_coordinates()[1] = ",self.get_current_coordinates()[1])
                    # print("self.get_current_coordinates()[2] = ",self.get_current_coordinates()[2])
                    # print("self.parse_g_code(g_code=g_code)['Z']",self.parse_g_code(g_code=g_code)['Z'])
                    # print()
                    if self.get_current_coordinates()[2] != self.parse_g_code(g_code=g_code)['Z']:
                        if self.get_current_coordinates()[2] > self.parse_g_code(g_code=g_code)['Z']:
                            # g_code += f" F{self.F_up}"
                            g_code = self.up_modifided_g_code(g_code=g_code)
                            named_tuple = time.localtime()
                            time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
                            print("move_without_M400 1.1: ", time_string)
                        else:
                            # g_code += f" F{self.F_down}"
                            g_code = self.down_modifided_g_code(g_code=g_code)
                            named_tuple = time.localtime()
                            time_string = time.strftime("%m/%d/%Y, %H:%M:%S", named_tuple)
                            print("move_without_M400 1.2: ", time_string)
        try:
            self.controller.send_to_robot_port(g_code)
            line = self.controller.read_from_robot_port()
        except serial.serialutil.SerialException:
            raise exceptions.RobotOfflineException

    def get_current_coordinates(self):
        while True:
            self.controller.send_to_robot_port("M114.2\n")
            robot_line = self.controller.read_from_robot_port()
            if len(robot_line) > 0:
                try:
                    return [
                        round(float(robot_line.split(" ")[2][2:])),
                        round(float(robot_line.split(" ")[3][2:])),
                        round(float(robot_line.split(" ")[4][2:]))
                    ]
                except Exception:
                    continue

    def move_to_washing_machine(self, box_height, column_z, lengthToSensor, sensor, max_height_line1, column):
        print("\n")
        print("НАЧАЛО move_to_washing_machine")
        if column['id'] == 1 or column['id'] == 2: #если вторая линия
            if column_z > max_height_line1: #высота второй линии меньше высоты 1 линии (от пола)
                print("высота второй линии меньше высоты 1 линии (от пола)")
                # событие 1
                # if column['z'] < 1850: #ящики вверху, т.к. меньше 1800
                if (max_height_line1 - lengthToSensor - box_height - 200) < (1520 - box_height - 30): #ящики вверху, т.к. меньше 1800
                    print("move_to_washing_machine Событие 1 ящик сверху")
                    self.move_without_M400(f"G1 Z{max_height_line1 - lengthToSensor - box_height - 200}")
                    self.scan_while_move(g_code=f"G1 X{self.start_X} Y{self.start_Y}", sensor=sensor)
                    self.scan_while_move(g_code=f"G1 Z{1520 - box_height - 30}", sensor=sensor)
                # событие 2
                else: #ящики внизу, т.к. больше 1800
                    print("move_to_washing_machine Событие 2 ящик внизу")
                    self.move_without_M400(f"G1 Z{max_height_line1 - lengthToSensor - box_height - 200}")
                    # self.scan_while_move(g_code=f"G1 Y{654}", sensor=sensor)
                    self.scan_while_move(g_code=f"G1 X{self.start_X} Y{self.start_Y} Z{1520 - box_height - 30} ",sensor=sensor)

            elif column_z <= max_height_line1: #высота второй линии больше высоты 1 линии (от пола)
                print("высота второй линии больше высоты 1 линии (от пола)")
                # событие 3
                # if column_z < 1800: #ящики вверху, т.к. меньше 1800
                if (max_height_line1 - lengthToSensor - box_height - 200) < (1520 - box_height - 30): #ящики вверху, т.к. меньше 1800
                    print("move_to_washing_machine Событие 3 ящик сверху")
                    self.scan_while_move(g_code=f"G1 Z{column_z - lengthToSensor - box_height - 200}", sensor=sensor)
                    self.scan_while_move(g_code=f"G1 X{self.start_X} Y{self.start_Y}", sensor=sensor)
                    self.scan_while_move(g_code=f"G1 Z{1520 - box_height - 30}", sensor=sensor)
                # событие 4
                else: #ящики внизу, т.к. больше 1800
                    print("move_to_washing_machine Событие 4 ящик снизу")
                    self.scan_while_move(g_code=f"G1 Z{column_z - lengthToSensor - box_height - 200}", sensor=sensor)
                    self.scan_while_move(g_code=f"G1 X{self.start_X} Y{self.start_Y} Z{1520 - box_height - 30} ", sensor=sensor)

        elif column['id'] == 3 or column['id'] == 4: #если первая линия
            print("Работаенм с первой линией")
            if (max_height_line1 - lengthToSensor - box_height - 200) <  (1520 - box_height - 30): #ящики вверху, т.к. меньше 1800
                print("move_to_washing_machine 1 линия ящик сверху")
                self.scan_while_move(g_code=f"G1 Z{column_z - lengthToSensor - box_height - 200}", sensor=sensor)
                self.scan_while_move(g_code=f"G1 X{self.start_X} Y{self.start_Y}", sensor=sensor)
                self.scan_while_move(g_code=f"G1 Z{1520 - box_height - 30}", sensor=sensor)

            else: #ящики внизу, т.к. больше 1800
                print("move_to_washing_machine 1 линия ящик снизу")
                self.scan_while_move(g_code=f"G1 Z{column_z - lengthToSensor - box_height - 200}", sensor=sensor)
                self.scan_while_move(g_code=f"G1 X{self.start_X} Y{self.start_Y} Z{1520 - box_height - 30}",sensor=sensor)

        # толкаем ящик
        self.move_without_M400(f"G1 Y{1780} F{self.F_xy}")
        # Ждем завершения цикла
        # self.move("M400")
        self.scan_with_M400(sensor=sensor)

        self.box_in_washing_machine = True  # Ящик доставлен отключаем сканирование

        # РАЗЖАТЬ ЗАХВАТ
        self.move_without_M400("M40.2")
        self.move_without_M400("M40.3")
        self.move_without_M400("M40.4")
        self.move_without_M400("M40.1")  # вЫключили присоски
        self.move_without_M400("G4P300")

        #Проверить расчет времени цикла.
        # if self.start_time != 0:
        #     print("время ", time.time() - self.start_time)
        # if time.time() < self.t0:
        #     # time.sleep(self.t0 - time.time())
        #     print("Спим")
        #
        # self.t0 = time.time() + self.first_time_box_output
        # self.start_time = time.time()

        # self.move_without_M400(f"G1 Y{1580} F{self.F_xy}")

        self.move("M400")
        self.box_in_washing_machine = False
        print("КОНЕЦ move_to_washing_machine")
        print("\n")

    @staticmethod
    def parse_g_code(g_code):
        result = {}
        for i in g_code.split(" "):
            if "X" in i:
                result['X'] = float(i[1:])
            if "Y" in i:
                result['Y'] = float(i[1:])
            if "Z" in i:
                result['Z'] = float(i[1:])
        return result

    def scan_with_M400(self, sensor, with_scan=True):
        self.controller.send_to_robot_port('M400')
        response = ""
        while True:
            response = self.controller.read_from_robot_port()
            if "M400ok" in response:
                break
            if with_scan:
                is_not_hooked = sensor.box_is_hooked()
                print(f"is_hooked - {is_not_hooked}")
                if is_not_hooked == 404:
                    continue
                    # raise exceptions.SensorOfflineException
                else:
                    if is_not_hooked:  # Если ящик не зацеплен

                        self.emgs_stop()
                        self.move_without_M400("M40.1")  # вЫключили присоски ПРОВЕРИТЬ ВЕРОЯТНО РАБОТАЕТ НЕ ПРАВИЛЬНО
                        self.move_without_M400("M40.2")  # вЫключили присоски ПРОВЕРИТЬ ВЕРОЯТНО РАБОТАЕТ НЕ ПРАВИЛЬНО
                        self.move_without_M400("M40.3")  # вЫключили присоски ПРОВЕРИТЬ ВЕРОЯТНО РАБОТАЕТ НЕ ПРАВИЛЬНО
                        self.move_without_M400("M40.4")  # вЫключили присоски ПРОВЕРИТЬ ВЕРОЯТНО РАБОТАЕТ НЕ ПРАВИЛЬНО
                        self.controller.box_fell_down_error()
                        while True:
                            try:
                                self.controller.send_to_robot_port("M114.2")
                            except serial.serialutil.SerialException:
                                raise exceptions.RobotOfflineException

    def emgs_stop(self):
        self.controller.robot_port.write(b'\x18')
