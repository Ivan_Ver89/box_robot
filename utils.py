import time
from threading import Thread
import serial
import asyncio

import exceptions

BOXES_TYPES = {
    1: {"L": 600, "W": 400, "H": 115, "delta": 115, "gripper_g_code": ""},
    2: {"L": 600, "W": 400, "H": 188, "delta": 188, "gripper_g_code": ""},
    3: {"L": 600, "W": 400, "H": 183, "delta": 183, "gripper_g_code": ""},
    4: {"L": 600, "W": 400, "H": 108.5, "delta": 108.5, "gripper_g_code": ""},  # проверен
    5: {"L": 600, "W": 400, "H": 238.9, "delta": 238.9, "gripper_g_code": ""},  # проверен 215 9ящ = 238.89
    6: {"L": 540, "W": 340, "H": 300, "delta": 80, "gripper_g_code": ""},
    7: {"L": 530, "W": 340, "H": 202, "delta": 58, "gripper_g_code": ""},
    8: {"L": 530, "W": 340, "H": 202, "delta": 58, "gripper_g_code": ""},
    # 9  зеленый с 2 дырками 8 штук 199см = 24.875
}

g_code = [
    # "G1 X350 Y754 F60000\nM400",
    # "G1 X350 Y704 F8000",
    # "G1 X350 Y354 F60000\nM400",
    # "G1 X350 Y304 F8000",
    # "G1 X950 Y304 F60000\nM400",
    # "G1 X950 Y354 F8000",
    # "G1 X950 Y704 F60000\nM400",
    # "G1 X950 Y754 F8000",
    "G1 X350 Y654 F40000",
    "G1 X370 Y604 F6000",
    "G1 X350 Y254 F40000",
    "G1 X370 Y204 F6000",
    "G1 X950 Y204 F40000",
    "G1 X930 Y254 F6000",
    "G1 X950 Y604 F40000",
    "G1 X930 Y654 F6000",
]

lengthToSensor = 223


def calculate_z(box_count, box_number, heights: list, zero_point):
    # return 2780 - (2780 - 400 -(150 + BOXES_TYPES[box_number]["H"] + (box_count - 1) * BOXES_TYPES[box_number]["delta"]))
    # BT_BN = BOXES_TYPES[box_number]["H"]
    # BT_BD = BOXES_TYPES[box_number]["delta"]

    # print("BT_BN", BT_BN ,"box_count",box_count,"BT_BD",BT_BD)
    # print("РЕЗУЛЬТАТ ",2780 - 150 - BOXES_TYPES[box_number]["H"] - (box_count - 1) * BOXES_TYPES[box_number]["delta"])
    # print("Высота прихода по датчикам",  min(heights)-225)

    # return 2915 - 150 - BOXES_TYPES[box_number]["H"] - (box_count - 1) * BOXES_TYPES[box_number]["delta"]
    return 2915 - 150 - BOXES_TYPES[box_number]["H"] - (box_count - 1) * BOXES_TYPES[box_number]["delta"]

    # return 2780
    # current_z = 2780 - (2780 - min([x['H'] for x in res])) - 400


def get_zero_point(sensor, controller):
    time.sleep(0.3)
    zero_point = 0
    counter = 0
    for i in range(0, 5):
        while zero_point == 0 or zero_point == None:
            zero_point = sensor.get_sensor_height()
            print("ZP - ", zero_point)
            time.sleep(0.01)
            counter += 1
            if counter == 150:
                controller.sensor_returns_nothing_error()
                while True:
                    try:
                        controller.robot_port.write("M114.2\n".encode('ascii'))
                    except serial.serialutil.SerialException:
                        raise exceptions.RobotOfflineException
    return zero_point


def pre_detect(sensor, robot, box_number, controller):
    robot.move("G1 Z0 A0")
    robot.go_to_start()
    count = 0
    zero_point = get_zero_point(sensor, controller)
    res = list()
    while True:
        for i, code in enumerate(g_code):
            if i % 2 == 0:
                robot.move(code)
            else:
                # robot.move_without_M400(code)
                res.extend(scan(g_code=code, robot=robot, sensor=sensor, controller=controller))
        print("Минимальное расстояние от датчика до ящика", min([x["H"] for x in res]))
        print("zero_point - Минимальная расстояние от датчика до ящика =", zero_point - min([x["H"] for x in res]))
        if zero_point - min([x["H"] for x in
                             res]) > 170:  # правильное значние 150 (паллет), на погрешность датчика закладываем 20 мм
            robot.go_to_start()
            # current_z = 2935 - (2935 - min([x['H'] for x in res])) - 400
            # current_z = 2938 - (2938 - min([x['H'] for x in res]) - 225) - 400
            current_z = min([x['H'] for x in res]) - lengthToSensor - 400
            # if current_z < 0:
            #     current_z = 0
            # else:
            #     pass

            print("высота от 0 робота до ящика", min([x['H'] for x in res]) + lengthToSensor)
            print("Минимальное расстоние от 0 до ящика", 750)

            print("Высота опускания перед сканированием", current_z)

            if (min([x['H'] for x in res]) + lengthToSensor - (223 + 200 + 300 + 27) >= 0) and (current_z > 0):
                # if min([x['H'] for x in res]) - lengthToSensor - 523 < 0:
                robot.move(f"G1 Z{current_z}")
                return current_z
            else:
                controller.high_error()
                return None

            # if current_z > 2300:
            #     return None
            # else:
            #     robot.move(f"G1 Z{current_z}")
            #     return current_z
        else:
            print("ящики не найдены")
            count += 1
            if count == 3:  # количество сканирований
                controller.boxes_not_found_error()
                return None


def detect_box(box_number, sensor, robot, controller):
    column = {
        #     0: {"id": 3, "z": None, "count": 10, "x": 460, "y": 620, "a": 180},  # левый верхний
        #     1: {"id": 1, "z": None, "count": 10, "x": 460, "y": 220, "a": 180},  # левый нижний
        #     2: {"id": 2, "z": None, "count": 10, "x": 1030, "y": 220, "a": 180},  # правый нижний
        #     3: {"id": 4, "z": None, "count": 10, "x": 1030, "y": 620, "a": 180},  # правый верхний

        0: {"id": 3, "z": None, "count": 10, "x": 430, "y": 610, "a": 180},  # левый верхний
        1: {"id": 1, "z": None, "count": 10, "x": 430, "y": 220, "a": 180},  # левый нижний ok
        2: {"id": 2, "z": None, "count": 10, "x": 1030, "y": 220, "a": 180},  # правый нижний
        3: {"id": 4, "z": None, "count": 10, "x": 1030, "y": 610, "a": 180},  # правый верхний
    }

    zero_point = get_zero_point(sensor, controller)
    print("zero_point", zero_point)
    j = 0
    robot.move_without_M400("G1 X695 Y900 F120000")
    for i, code in enumerate(g_code):
        if i % 2 == 0:
            robot.move(code)
        else:
            res = scan(g_code=code, robot=robot, sensor=sensor, controller=controller)
            # print("-----------------")
            # print("ЯЩИК ", j, " zero_point",zero_point)
            column[j]["count"] = get_box_count(heights=[x["H"] for x in res],
                                               box_number=box_number,
                                               zero_point=zero_point)
            # print("ЯЩИКОВ",column[j]["count"] )
            # print("id",column[j]["id"])
            column[j]["z"] = calculate_z(column[j]["count"], box_number, heights=[x["H"] for x in res],
                                         zero_point=zero_point)
            # print("Z ",column[j]["z"])
            # print(column)
            real_height = zero_point - min([x["H"] for x in res]) - 150
            # real_height = (max([x["H"] for x in res]) - min([x["H"] for x in res])) + 150

            calc_height = BOXES_TYPES[box_number]["H"] + BOXES_TYPES[box_number]["delta"] * (column[j]["count"] - 1)

            print("Количество ящиков ", column[j]["count"])
            print("zero_point ", zero_point)
            print("min ", min([x["H"] for x in res]))
            print("-------")
            print("Реальная высота ", real_height)
            print("Посчитанная высота ", calc_height)
            print("-------")
            print("abs(real_height - calc_height) > 42 ", abs(real_height - calc_height) > 42)
            print("real_height - calc_height =", real_height - calc_height)

            # print("Количество ящиков ", column[j]["count"] > 0)

            if column[j]["count"] > 0 and abs(real_height - calc_height) > 42:
                controller.wrong_box_type_error()
                while True:
                    try:
                        controller.robot_port.write("M114.2\n".encode())
                    except serial.serialutil.SerialException:
                        # raise exceptions.ReloadException
                        raise exceptions.RobotOfflineException
            j = j + 1

    robot.move("G1 X695 Y1130 F120000")
    robot.go_to_start()
    robot.move("G1 A180")
    robot.move_without_M400("G4P6000")
    return column


def scan(g_code, robot, sensor, controller):
    controller.scan_status()
    current_coords = None
    result = list()
    robot.move_without_M400(g_code)
    print("start scan")
    while current_coords != (float(g_code.split(" ")[1][1:]), float(g_code.split(" ")[2][1:])):
        current_coords = (robot.get_current_coordinates()[0], robot.get_current_coordinates()[1])
        height = sensor.get_sensor_height()
        if height and height != 0 and current_coords:
            result.append({
                "X": current_coords[0],
                "Y": current_coords[1],
                "H": height,
            })
        time.sleep(0.009)
    print("finish scan")
    controller.in_work_status()
    return result


def get_box_count(heights: list, box_number, zero_point):
    # print("Ящик ", " Высота до ящика ", min(heights))
    #
    print("---------------------------------------")

    if ((zero_point - min(heights) - 150 - BOXES_TYPES[box_number]['H']) / BOXES_TYPES[box_number]['delta'] + 1) <= 0:
        print("зашли", " округл кол-во ящиков: ", 0,
              " min(heights)", min(heights), " zero_point", zero_point, " Высота ящика", BOXES_TYPES[box_number]['H'],
              " Высота дельты ящика", BOXES_TYPES[box_number]['delta'])
        return 0
    else:
        print("зашли", " округл кол-во ящиков: ", int_r(
            (zero_point - min(heights) - 150 - BOXES_TYPES[box_number]['H']) / BOXES_TYPES[box_number]['delta'] + 1),
              " min(heights)", min(heights), " zero_point", zero_point, " Высота ящика", BOXES_TYPES[box_number]['H'],
              " Высота дельты ящика", BOXES_TYPES[box_number]['delta'])

        return int_r(
            (zero_point - min(heights) - 150 - BOXES_TYPES[box_number]['H']) / BOXES_TYPES[box_number]['delta'] + 1)


def int_r(num):
    num = num + 0.5
    num = int(num)
    return num


def combine_boxes(column, robot, box_number, sensor):
    print("ЗАБИРАЕМ combine_boxes")
    column_list = [
        column[0],
        column[1],
        column[2],
        column[3]
    ]
    while (column[0]["count"] + column[1]["count"] + column[2]["count"] + column[3]["count"]) > 0:
        column_list = sorted(column_list, key=lambda x: (-x["count"], x["id"]))
        current_column = column_list[0]  # Находит максимальную колонку по старому алгоритму
        # Находит соседа по Х
        current_y_neighbor = 0
        max_height_line1 = 0
        print(f"Матрица до сбора")
        matrix_print(column)
        print(f"нашли current_column['id'] - {current_column['id']} - {current_column['z']}")
        if current_column['id'] == 3 or current_column['id'] == 4:
            # print(f"текущая лини1, 3 или 4 колонка = {current_column['id']}")
            current_y_neighbor = [x for x in column_list if x['x'] == current_column['x'] and
                                  x['y'] != current_column['y']][0]
            # Если разница между соседями по Y меньши высоты выбранного ящика
            # Если разница между соседями по Y меньши высоты выбранного ящика
            if abs(current_column['z'] - current_y_neighbor['z']) <= BOXES_TYPES[box_number]['H'] and \
                    current_y_neighbor['count'] != 0:
                # print(f"differ - {abs(current_column['z'] - current_y_neighbor['z'])}")
                # То забираем соседа на второй линии
                print(f"меняем на current_y_neighbor - {current_y_neighbor['id']} - {current_y_neighbor['z']}")
                max_height_line1 = current_column["z"]  # Высчитывает высоту максимальной стопки в line1
                current_column = current_y_neighbor
                print(f"БЫЛА ЗАМЕНА")
            else:
                max_height_line1 = current_column["z"]
        elif current_column['id'] == 1 or current_column['id'] == 2:
            max_height_line1 = current_column["z"]

        print(f"max_height_line1 {max_height_line1}")



        lust_layer = False
        if (column[0]["count"] == 1 or column[0]["count"] == 0) and (
                column[1]["count"] == 1 or column[1]["count"] == 0) and (
                column[2]["count"] == 1 or column[2]["count"] == 0) and (
                column[3]["count"] == 1 or column[3]["count"] == 0):
            lust_layer = True

        activate_fourth_gripper = False
        if current_column["id"] == 2:
            if current_column["count"] > [i for i in column_list if i["id"] == 1][0]["count"]:
                activate_fourth_gripper = True

        capture_one_box(current_column, robot, box_number, lust_box=lust_layer, sensor=sensor,
                        max_height_line1=max_height_line1, activate_fourth_gripper=activate_fourth_gripper)

        # if current_column["count"] != 1:
        current_column["z"] += BOXES_TYPES[box_number]["delta"]
        current_column["count"] -= 1

        print(f"Матрица после сбора")
        matrix_print(column)
        print('\n')

    print("завершил combine_boxes")


def capture_one_box(column, robot, box_number, lust_box, sensor, max_height_line1, activate_fourth_gripper):
    # print("Ящик ", column["id"], " Высота прихода по размеру ящика ", column['z'] - lengthToSensor - 200)
    # if column['z'] < 1500 - BOXES_TYPES[box_number]['H']:
    start = time.time()

    print("\n")
    print("capture_one_box начало")

    print("column['id']", column['id'])
    print("column['z']", column['z'])

    if column['id'] == 1 or column['id'] == 2:  # если вторая линия
        if column['z'] > max_height_line1:  # высота второй линии меньше высоты 1 линии (от пола)

            if column['z'] < 1800:  # #событие 1
                print("событие 1 Высота ", column['z'], "ящики вверху, т.к. меньше 1800")
                robot.move_without_M400(f"G1 X{695} Y{1480} Z{max_height_line1 - lengthToSensor - 200}")
                robot.move_without_M400(f"G1 X{column['x']} Y{column['y']}")
                robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{column['z'] - lengthToSensor - 200}")

            else:  # событие 2
                print("событие 2 Высота ", column['z'], "ящики внизу, т.к. больше 1800")
                robot.move_without_M400(f"G1 X{695} Y{1130} Z{1520 - BOXES_TYPES[box_number]['H']}")
                robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{max_height_line1 - lengthToSensor - 200}")
                robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{column['z'] - lengthToSensor - 200}")

        elif column['z'] <= max_height_line1:  # высота второй линии больше высоты 1 линии (от пола)

            if column['z'] < 1800:  # событие 3
                print("событие 3 Высота ", column['z'], "ящики вверху, т.к. меньше 1800")
                robot.move_without_M400(f"G1 X{695} Y{1480} Z{column['z'] - lengthToSensor - 200}")
                robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{column['z'] - lengthToSensor - 200}")
            else:  # событие 4
                print("событие 4 Высота ", column['z'], "ящики внизу, т.к. больше 1800")
                robot.move_without_M400(f"G1 X{695} Y{1130} Z{1520 - BOXES_TYPES[box_number]['H']}")
                robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{column['z'] - lengthToSensor - 200}")

    elif column['id'] == 3 or column['id'] == 4:  # если первая линия
        if column['z'] < 1800:
            print("Высота ", column['z'], "ящики вверху, т.к. меньше 1800")
            robot.move_without_M400(f"G1 X{695} Y{1480} Z{column['z'] - lengthToSensor - 200}")
            robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{column['z'] - lengthToSensor - 200}")
        else:
            print("Высота ", column['z'], "ящики внизу, т.к. больше 1800")
            robot.move_without_M400(f"G1 X{695} Y{1130} Z{1520 - BOXES_TYPES[box_number]['H']}")
            robot.move_without_M400(f"G1 X{column['x']} Y{column['y']} Z{column['z'] - lengthToSensor - 200}")

    print("ВЫСОТА Ящика", column['z'])
    print("ВЫСОТА ОПУСКАНИЯ подхода к ящику", column['z'] - lengthToSensor - 200)
    print("ВЫСОТА ОПУСКАНИЯ захват к ящику", column['z'] - lengthToSensor - 70)

    robot.move_without_M400(f"G1 Z{column['z'] - lengthToSensor - 70}")
    robot.scan_with_M400(sensor=sensor, with_scan=False)

    #    ХВАТАЕМ
    # ------------------ зеленый конусный - присоски + поддув ------------------
    if box_number == 6:
        # print("Ящик 6")
        # print(column["id"])
        # последний ящик
        # if lust_box == True:
        if column["count"] == 1:
            robot.move_without_M400("M41.1")  # включили присоски
            # robot.move_without_M400(f"M41.2")
            robot.move_without_M400(f"M41.3")  # правый нижний
            robot.move_without_M400("G4P200")  # пауза
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000", sensor=sensor)



        else:
            #    ХВАТАЕМ
            if column["id"] == 1:
                robot.move_without_M400(f"M41.4")  # поддув
                robot.move_without_M400(f"M41.2")
                robot.move_without_M400("G4P600")
                step = stepper(robot, column['z'], lengthToSensor, step_up=5, step_down=3, target=65)
                robot.move_without_M400(f"M40.4")  # поддув
                robot.scan_while_move(
                    g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                    sensor=sensor)

            elif column["id"] == 3:
                robot.move_without_M400(f"M41.4")  # поддув
                robot.move_without_M400(f"M41.2")
                robot.move_without_M400("G4P600")
                step = stepper(robot, column['z'], lengthToSensor, step_up=5, step_down=3, target=65)
                robot.move_without_M400(f"M40.4")  # поддув
                robot.scan_while_move(
                    g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                    sensor=sensor)

            elif column["id"] == 2:
                robot.move_without_M400(f"M41.4")  # поддув
                robot.move_without_M400(f"M41.3")
                robot.move_without_M400("G4P600")
                step = stepper(robot, column['z'], lengthToSensor, step_up=5, step_down=3, target=65)
                robot.move_without_M400(f"M40.4")  # поддув
                robot.scan_while_move(
                    g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                    sensor=sensor)

            else:
                robot.move_without_M400(f"M41.4")  # поддув
                robot.move_without_M400(f"M41.3")
                robot.move_without_M400("G4P600")
                step = stepper(robot, column['z'], lengthToSensor, step_up=5, step_down=3, target=65)
                robot.move_without_M400(f"M40.4")  # поддув
                robot.scan_while_move(
                    g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                    sensor=sensor)

    # ------------------ прямые и конусные гладкие - с присосками: 2 синий красный 5 синий красный 1 красный 8 синий------------------
    if box_number == 2 or box_number == 5 or box_number == 1 or box_number == 8:  # прямые с присосками
        if column["id"] == 1:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

        elif column["id"] == 3:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

        elif column["id"] == 2:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400(f"M41.3")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

        elif column["id"] == 4:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400(f"M41.3")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

    # ------------------ прямые и конусные с перфорацией - без присосок: 3 зеленый, 3 зеленый, 7 желтый------------------
    if box_number == 3 or box_number == 4 or box_number == 7:
        if column["id"] == 1:
            # if lust_box == True:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

        elif column["id"] == 3:
            # if lust_box == True:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

        elif column["id"] == 2:
            # if lust_box == True:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400(f"M41.3")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

        elif column["id"] == 4:
            # if lust_box == True:
            robot.move_without_M400(f"M41.2")
            robot.move_without_M400(f"M41.3")
            robot.move_without_M400("G4P600")
            robot.scan_while_move(
                g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']} F60000",
                sensor=sensor)

    # robot.move_without_M400(
    #     f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']}")

    # robot.scan_while_move(g_code=f"G1 Z{column['z'] - lengthToSensor - 200 - BOXES_TYPES[box_number]['H']}", sensor=sensor)

    robot.move_to_washing_machine(box_height=BOXES_TYPES[box_number]['H'], column_z=column['z'],
                                  lengthToSensor=lengthToSensor, sensor=sensor, max_height_line1=max_height_line1,
                                  column=column)

    if lust_box:
        pass
    print("Время цикла ", time.time() - start)
    print("capture_one_box конец")
    print("\n")


def stepper(robot, column_z, lengthToSensor, step_up, step_down, target):
    res = 0
    while res <= target:
        res += step_up
        if res >= target:
            return res
        robot.move_without_M400(f"G1 Z {column_z - lengthToSensor - 70 - 25 - res} F90000")
        robot.move_without_M400("G4P100")
        res -= step_down
        if res > target:
            return res
        robot.move_without_M400(f"G1 Z {column_z - lengthToSensor - 70 - 25 - res} F90000")
        robot.move_without_M400("G4P100")


def matrix_print(col):
    print(f"| {col[0]['count']} ({col[0]['z']}) |  {col[3]['count']} ({col[3]['z']})  |")
    print(f"| {col[1]['count']} ({col[1]['z']})  |  {col[2]['count']} ({col[2]['z']})  |")
