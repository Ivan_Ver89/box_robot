import asyncio
import re
import time


class Sensor:
    pattern = "L\d{1,} P\d{1}"

    def run(self, controller=None):
        self.controller = controller

    def ping_port(self):
        self.controller.read_from_sensor_port()

    def get_sensor_height(self):
        self.controller.read_from_sensor_port()
        # #
        time.sleep(0.04)
        # sensor_line = self.controller.read_from_sensor_port(line=True)
        sensor_line = self.controller.read_from_sensor_port()
        # print(f'sensor line {sensor_line}')
        if sensor_line:
            try:
                sensor_line = sensor_line.decode()
                # sl = sensor_line.replace("\n", " ")
                # print(f'sensor line {sl}')
                res = re.findall(self.pattern, sensor_line)
                if len(res) > 0:
                    # print("res ", res[-1].split(" ")[1])
                    # return float(res[-1].split(" ")[1])
                    return float(res[-1].split(" ")[0][1:])
            except Exception:
                return 0

    def box_is_hooked(self):
        self.controller.read_from_sensor_port()
        time.sleep(0.04)
        sensor_line = self.controller.read_from_sensor_port()
        # print(f'sensor line box_is_hooked: {sensor_line}')
        if sensor_line:
            try:
                sensor_line = sensor_line.decode()
                return "P0" in sensor_line  # Если не равно Р1, то ящик висит в зацепе
            except Exception:
                return 404
